:source-highlighter: coderay
:toc:

= L'API - Ecriture des contrats de services

Nous venons d'identifier les web-services REST à développer. Nous allons maintenant les déclarer dans notre application, en rédigeant les *contrats de services* correspondants.

== Qu'est-ce qu'un contrat de service ?

Un contrat de service est un fichier structuré (dans notre cas, au format *YAML*) qui sert à définir l'ensemble des services REST d'une API, c'est à dire :

* leur méthode HTTP
* leur URL
* leurs paramètres et données d'entrée
* leur format de réponse

...c'est à dire, tout ce que nous avons défini dans link:3_4-api_identification_services.html[la partie précédente].

Ce fichier, lu par un plugin Maven (*OpenAPI*), va permettre la génération automatique des fichiers Java :

* des interfaces de définition des méthodes correspondant à chaque service
* des objets associés, utiles aux données d'entrée et de réponse

C'est à partir de ces éléments que nous pourrons ensuite implémenter l'ensemble des services REST souhaités.

[NOTE]
====
Il est tout à fait possible de ne pas utiliser de plugin Maven de génération automatique de fichiers Java, et de réaliser ces éléments soi-même. Mais l'utilisation d'OpenAPI permet :

* des développements plus rapides (car une grande partie du code est générée)
* un partage des contrats YAML, à des interfaces clientes utilisant elles-mêmes la génération de code à partir du même format (par exemple avec *Angular*...)
* un contrat de service suffisamment complet peut également faire office de documentation, ce qui évite au développeur de reporter les mêmes informations dans des documents "bureautiques" (Word, etc...)
====

== Initialisation du fichier *openapi_microblog.yaml*

En premier lieu, créer un dossier `openapi` dans `microblog-api/src/main/resources` et y ajouter un fichier `openapi_microblog.yaml`. En voici le contenu initial :

[source,yaml]
----
# Définition de la version d'OpenAPI
openapi: 3.0.3

# Informations sur l'API
info: 
  title: 'Microblog - API'
  version: '1.0'

# Informations sur le serveur
servers: 
  - http://localhost:8080/microblog

# Définition des services
paths: 
  # ...
  
# Définition des objets associés
components:
  schemas:
    # ...
----

[WARNING]
====
Ne jamais utiliser de tabulation de un fichier *YAML*. Seuls deux caractères *espace* font office d'identation. 
====

== Définition des services et objets associés

Reporter dans `openapi_microblog.yaml` les services REST définis et les objets mentionnés dans link:3_4-api_identification_services.html[la partie précédente].

Le fichier `openapi_microblog.yaml` final est disponible https://gitlab.com/tgoubin/java-web-tutorial/-/blob/master/microblog-api/src/main/resources/openapi/openapi_microblog.yaml[ici].

== Génération du code Java par *OpenAPI*

=== Configuration maven

* dans le `pom.xml` du projet *microblog-api*, ajouter les dépendances suivantes :

[source,xml]
----
<dependency>
	<groupId>io.swagger</groupId>
	<artifactId>swagger-core</artifactId>
	<version>${swagger.version}</version>
</dependency>
<dependency>
	<groupId>io.springfox</groupId>
	<artifactId>springfox-swagger2</artifactId>
	<version>${springfox-swagger.version}</version>
</dependency>
<dependency>
	<groupId>org.openapitools</groupId>
	<!-- Pour éviter d'écrirer les valeurs nulles dans les réponses HTTP -->
	<artifactId>jackson-databind-nullable</artifactId>
	<version>${jackson.databind.nullable.version}</version>
</dependency>
----

* toujours dans le `pom.xml` du projet *microblog-api*, ajouter les plugins nécessaires :

[source,xml]
----
<build>
	<plugins>
		<!-- Pour générer les classes Java à partir des contrats de services YAML -->
		<plugin>
			<groupId>org.openapitools</groupId>
			<artifactId>openapi-generator-maven-plugin</artifactId>
			<executions>
				<execution>
					<goals>
						<goal>generate</goal>
					</goals>
					<configuration>
						<inputSpec>${project.basedir}/src/main/resources/openapi/openapi_microblog.yaml</inputSpec>
						<language>spring</language>
						<apiPackage>com.javawebtutoriel.microblog.api</apiPackage>
						<modelPackage>com.javawebtutoriel.microblog.api.dto</modelPackage>
						<output>${project.build.directory}/generated-sources/openapi</output>
						<generatorName>spring</generatorName>
						<generateModelTests>false</generateModelTests>
						<generateModelDocumentation>false</generateModelDocumentation>
						<generateApiDocumentation>false</generateApiDocumentation>
						<generateApiTests>false</generateApiTests>
						<generateSupportingFiles>true</generateSupportingFiles>
						<supportingFilesToGenerate>ApiUtil.java</supportingFilesToGenerate>
						<configOptions>
							<dateLibrary>java8</dateLibrary>
							<interfaceOnly>true</interfaceOnly>
						</configOptions>
					</configuration>
				</execution>
			</executions>
		</plugin>
		<!-- Pour ajouter les classes Java générées aux sources Eclipse -->
		<plugin>
			<groupId>org.codehaus.mojo</groupId>
			<artifactId>build-helper-maven-plugin</artifactId>
			<version>${build.helper.maven.plugin.version}</version>
			<executions>
				<execution>
					<phase>generate-sources</phase>
					<goals>
						<goal>add-source</goal>
					</goals>
					<configuration>
						<sources>
							<source>${project.build.directory}/generated-sources/openapi/src/main/java</source>
						</sources>
					</configuration>
				</execution>
			</executions>
		</plugin>
	</plugins>
</build>
----

* dans le `pom.xml` parent du projet *microblog*, déclarer les propriétés suivantes :

[source,xml]
----
<swagger.version>1.6.1</swagger.version>
<springfox-swagger.version>3.0.0</springfox-swagger.version>
<jackson.databind.nullable.version>0.2.1</jackson.databind.nullable.version>
<build.helper.maven.plugin.version>3.0.0</build.helper.maven.plugin.version>
----

=== Lancement de la génération de code

Lancer la commande `mvn clean package` à la racine du dossier *microblog*. Si elle est exécutée avec succès, un dossier `target/generated-sources/openapi/src/main/java` doit apparaître dans le projet *microblog-api*, contenant les éléments suivants :

[plantuml, "recap-openapi", png]  
----
@startsalt

{
	{T
		+ microblog
		++ microblog-api
		+++ target/generated-sources/openapi/src/main/java
		++++ com.javawebtutoriel.microblog.api
		+++++ ApiUtil.java
		+++++ LoginApi.java
		+++++ MessageApi.java
		+++++ UtilisateurApi.java
		++++ com.javawebtutoriel.microblog.dto
		+++++ Authentification.java
		+++++ Message.java
		+++++ ReactionMessage.java
		+++++ Utilisateur.java
	}
}

@endsalt
----

[NOTE]
====
A chaque modification du fichier `openapi_microblog.yaml`, une regénération du code Java par OpenAPI est nécessaire.
====

== Récapitulatif du contenu du projet *microblog*

[plantuml, "recap-3_5", png]  
----
@startsalt

{
	{T
		+ microblog
		++ microblog-api
		+++ src/main/java
		++++ com.javawebtutoriel.microblog.api
		+++++ MicroblogRestApplication.java
		+++ src/main/resources
		++++ openapi
		+++++ openapi_microblog.yaml
		++++ application.properties
		++++ logback.xml
		+++ target/generated-sources/openapi/src/main/java (generé)
		++++ com.javawebtutoriel.microblog.api (generé)
		+++++ ApiUtil.java (generé)
		+++++ LoginApi.java (generé)
		+++++ MessageApi.java (generé)
		+++++ UtilisateurApi.java (generé)
		++++ com.javawebtutoriel.microblog.dto (generé)
		+++++ Authentification.java (generé)
		+++++ Message.java (generé)
		+++++ ReactionMessage.java (generé)
		+++++ Utilisateur.java (generé)
		+++ pom.xml
		++ microblog-core
		+++ src/main/java
		++++ com.javawebtutoriel.microblog.core
		+++++ com.javawebtutoriel.microblog.core.common
		++++++ AbstractEntity.java
		+++++ com.javawebtutoriel.microblog.core.configuration
		++++++ MicroblogSpringConfiguration.java
		+++++ com.javawebtutoriel.microblog.core.message
		++++++ MessageEntity.java
		++++++ MessageRepository.java
		++++++ MessageService.java
		+++++ com.javawebtutoriel.microblog.core.reactionmessage
		++++++ ReactionMessageEntity.java
		++++++ ReactionMessageEnum.java
		+++++ com.javawebtutoriel.microblog.core.utilisateur
		++++++ UtilisateurEntity.java
		++++++ UtilisateurRepository.java
		++++++ UtilisateurService.java
		+++ pom.xml
		++ microblog-database
		+++ src/main/sql
		++++ 0_structure.sql
		+++ pom.xml
		++ pom.xml
	}
}

@endsalt
----
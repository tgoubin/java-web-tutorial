package com.goubinthibault.microblog.api.security;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.goubinthibault.microblog.api.message.MessageController;
import com.goubinthibault.microblog.api.utilisateur.UtilisateurController;

/**
 * Spring configuration for security
 * 
 * @author Thibault GOUBIN
 */
@Configuration
// The SpringBoot application enables the security
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityConfiguration.class);

	/**
	 * The security authentication filter
	 */
	private SecurityAuthenticationFilter securityAuthenticationFilter;

	/**
	 * If the CORS is enabled
	 */
	@Value("${security.cors.enabled}")
	private boolean isCorsActive;

	/**
	 * Constructor used by Spring to instanciate other Spring components used in
	 * this service
	 * 
	 * @param securityAuthenticationFilter the security authentication filter
	 */
	public SecurityConfiguration(SecurityAuthenticationFilter securityAuthenticationFilter) {
		this.securityAuthenticationFilter = securityAuthenticationFilter;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		HttpSecurity security = http.csrf().disable();

		if (isCorsActive) {
			security = security.cors().and();
		} else {
			http.csrf().disable().cors();
		}
	}

	@Bean
	public FilterRegistrationBean<SecurityAuthenticationFilter> filterRegistrationBean() {
		LOGGER.info("URLs to secure configuration");

		// Configuration of URLs to secure
		FilterRegistrationBean<SecurityAuthenticationFilter> filterRegistrationBean = new FilterRegistrationBean<>();
		filterRegistrationBean.setFilter(securityAuthenticationFilter);

		// Message API
		filterRegistrationBean.addUrlPatterns(MessageController.API_PATH);
		filterRegistrationBean.addUrlPatterns(MessageController.API_PATH + "/*");

		// Utilisateur API
		filterRegistrationBean.addUrlPatterns(UtilisateurController.API_PATH);
		filterRegistrationBean.addUrlPatterns(UtilisateurController.API_PATH + "/*");

		return filterRegistrationBean;
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		// CORS configuration
		CorsConfiguration configuration = new CorsConfiguration();

		if (isCorsActive) {
			LOGGER.info("CORS is active");

			// All origins are allowed
			configuration.setAllowedOrigins(Arrays.asList("*"));

			// HTTP methods allowed
			configuration.setAllowedMethods(
					Arrays.asList(HttpMethod.GET.name(), HttpMethod.POST.name(), HttpMethod.PUT.name(),
							HttpMethod.DELETE.name(), HttpMethod.PATCH.name(), HttpMethod.OPTIONS.name()));

			// Credentials allowed
			configuration.setAllowCredentials(true);

			// HTTP headers allowed
			configuration.setAllowedHeaders(
					Arrays.asList(HttpHeaders.AUTHORIZATION, HttpHeaders.CACHE_CONTROL, HttpHeaders.CONTENT_TYPE));
		} else {
			LOGGER.info("CORS is inactive");
		}

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
}

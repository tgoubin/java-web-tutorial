package com.goubinthibault.microblog.api.utilisateur;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.goubinthibault.microblog.api.UtilisateurApi;
import com.goubinthibault.microblog.api.dto.Utilisateur;
import com.goubinthibault.microblog.core.exception.AbstractException;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurService;

/**
 * REST controller for UtilisateurApi
 * 
 * @author Thibault GOUBIN
 */
//This class is a REST controller
@Controller
public class UtilisateurController implements UtilisateurApi {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilisateurController.class);

	/**
	 * Service for "utilisateur"
	 */
	private UtilisateurService utilisateurService;

	/**
	 * API path
	 */
	public static final String API_PATH = "/utilisateur";

	/**
	 * Constructor used by Spring to instanciate other Spring components used in
	 * this controller
	 * 
	 * @param utilisateurService service for "utilisateur"
	 */
	public UtilisateurController(UtilisateurService utilisateurService) {
		this.utilisateurService = utilisateurService;
	}

	/**
	 * Implementation of GET /utilisateur/{idUtilisateur}
	 * 
	 * @param idUtilisateur the utilisateur id
	 * @return the HTTP response (utilisateur or error)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ResponseEntity getUtilisateurById(Integer idUtilisateur) {
		LOGGER.info("GET {}/{}", API_PATH, idUtilisateur);

		try {
			// The REST service returns the HTTP response containing the UtilisateurService
			// response with an HTTP status OK
			return new ResponseEntity<Utilisateur>(
					UtilisateurMapper.entityToDto(utilisateurService.getById(idUtilisateur)), HttpStatus.OK);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * Implementation of POST /utilisateur
	 * 
	 * @param utilisateur the utilisateur
	 * @return the HTTP response (utilisateur or error)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity postUtilisateur(Utilisateur utilisateur) {
		LOGGER.info("POST {}", API_PATH);

		try {
			// The REST service returns the HTTP response containing the UtilisateurService
			// response with an HTTP status CREATED
			return new ResponseEntity<Utilisateur>(UtilisateurMapper.entityToDto(
					utilisateurService.create(UtilisateurMapper.dtoToEntity(utilisateur))), HttpStatus.CREATED);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * Implementation of PUT /utilisateur/{idUtilisateur}
	 * 
	 * @param idUtilisateur the utilisateur id
	 * @param utilisateur   the utilisateur
	 * @return the HTTP response (utilisateur or error)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity putUtilisateur(Integer idUtilisateur, Utilisateur utilisateur) {
		LOGGER.info("PUT {}/{}", API_PATH, idUtilisateur);

		try {
			// The REST service returns the HTTP response containing the UtilisateurService
			// response with an HTTP status OK
			return new ResponseEntity<Utilisateur>(
					UtilisateurMapper.entityToDto(
							utilisateurService.update(idUtilisateur, UtilisateurMapper.dtoToEntity(utilisateur))),
					HttpStatus.OK);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * Implementation of PUT /utilisateur/{idUtilisateur}/motDePasse
	 * 
	 * @param idUtilisateur the utilisateur id
	 * @param motDePasse    the mot de passe
	 * @return the HTTP response (void or error)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity putUtilisateurMotDePasse(Integer idUtilisateur, String motDePasse) {
		LOGGER.info("PUT {}/{}/motDePasse", API_PATH, idUtilisateur);

		try {
			// Utilisateur deleting
			utilisateurService.updateMotDePasse(idUtilisateur, motDePasse);

			// The REST service returns the HTTP response containing an HTTP status NO
			// CONTENT
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * Implementation of DELETE /utilisateur/{idUtilisateur}
	 * 
	 * @param idUtilisateur the utilisateur id
	 * @return the HTTP response (void or error)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity deleteUtilisateur(Integer idUtilisateur) {
		LOGGER.info("DELETE {}/{}", API_PATH, idUtilisateur);

		try {
			// Utilisateur deleting
			utilisateurService.delete(idUtilisateur);

			// The REST service returns the HTTP response containing an HTTP status NO
			// CONTENT
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}
}

package com.goubinthibault.microblog.api.message;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.goubinthibault.microblog.api.MessageApi;
import com.goubinthibault.microblog.api.dto.Message;
import com.goubinthibault.microblog.api.dto.ReactionMessage;
import com.goubinthibault.microblog.core.exception.AbstractException;
import com.goubinthibault.microblog.core.message.MessageConstants;
import com.goubinthibault.microblog.core.message.MessageService;
import com.goubinthibault.microblog.core.message.reaction.ReactionMessageEnum;

/**
 * REST controller for MessageApi
 * 
 * @author Thibault GOUBIN
 */
//This class is a REST controller
@Controller
public class MessageController implements MessageApi {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);

	/**
	 * Service for "message"
	 */
	private MessageService messageService;

	/**
	 * API path
	 */
	public static final String API_PATH = "/message";

	/**
	 * Constructor used by Spring to instanciate other Spring components used in
	 * this controller
	 * 
	 * @param messageService service for "message"
	 */
	public MessageController(MessageService messageService) {
		this.messageService = messageService;
	}

	/**
	 * Implementation of GET /message?tag={tags}
	 * 
	 * @param tags the tags to search
	 * @return the HTTP response (list or error)
	 */
	@Override
	public ResponseEntity<List<Message>> getMessages(String tags) {
		LOGGER.info("GET {}?tag={}", API_PATH, tags);

		// The REST service returns the HTTP response containing the MessageService
		// response with an HTTP status OK
		return new ResponseEntity<List<Message>>(
				MessageMapper.entitiesToDtos(
						messageService.get(new HashSet<>(Arrays.asList(tags.split(MessageConstants.TAGS_SEPARATOR))))),
				HttpStatus.OK);
	}

	/**
	 * Implementation of POST /message
	 * 
	 * @param message the message
	 * @return the HTTP response (message or error)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity postMessage(Message message) {
		LOGGER.info("POST {}", API_PATH);

		try {
			// The REST service returns the HTTP response containing the MessageService
			// response with an HTTP status CREATED
			return new ResponseEntity<Message>(
					MessageMapper.entityToDto(messageService.create(MessageMapper.dtoToEntity(message))),
					HttpStatus.CREATED);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * Implementation of PUT /message/{idMessage}
	 * 
	 * @param idMessage the message id
	 * @param message   the message
	 * @return the HTTP response (message or error)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity putMessage(Integer idMessage, Message message) {
		LOGGER.info("PUT {}/{}", API_PATH, idMessage);

		try {
			// The REST service returns the HTTP response containing the MessageService
			// response with an HTTP status OK
			return new ResponseEntity<Message>(
					MessageMapper.entityToDto(messageService.update(idMessage, MessageMapper.dtoToEntity(message))),
					HttpStatus.OK);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * Implementation of PUT /message/{idMessage}/reaction
	 * 
	 * @param idMessage       the message id
	 * @param reactionMessage the message reaction
	 * @return the HTTP response (message or error)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity putReactionMessage(Integer idMessage, ReactionMessage reactionMessage) {
		LOGGER.info("PUT {}/{}/reaction", API_PATH, idMessage);

		try {
			// The REST service returns the HTTP response containing the MessageService
			// response with an HTTP status OK
			return new ResponseEntity<Message>(MessageMapper.entityToDto(
					messageService.react(idMessage, ReactionMessageEnum.valueOf(reactionMessage.getReaction().name()))),
					HttpStatus.OK);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * Implementation of DELETE /message/{idMessage}
	 * 
	 * @param idUtilisateur the utilisateur id
	 * @return the HTTP response (void or error)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity deleteMessage(Integer idMessage) {
		LOGGER.info("DELETE {}/{}", API_PATH, idMessage);

		try {
			// Message deleting
			messageService.delete(idMessage);

			// The REST service returns the HTTP response containing an HTTP status NO
			// CONTENT
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}
}

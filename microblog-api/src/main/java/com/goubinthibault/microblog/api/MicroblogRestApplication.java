package com.goubinthibault.microblog.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Main class for Microblog REST SpringBoot Application
 * 
 * @author Thibault GOUBIN
 */
// The SpringBoot application will manage all classes in packages "com.goubinthibault.microblog.*"
@SpringBootApplication(scanBasePackages = "com.goubinthibault.microblog")
public class MicroblogRestApplication extends SpringBootServletInitializer {

	/**
	 * Main method
	 * 
	 * @param args the program arguments (no argument in this case)
	 */
	public static void main(String[] args) {
		// Launches the SpringBoot Microblog REST Application
		SpringApplication.run(MicroblogRestApplication.class);
	}
}

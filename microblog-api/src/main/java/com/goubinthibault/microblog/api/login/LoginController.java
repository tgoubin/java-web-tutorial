package com.goubinthibault.microblog.api.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.goubinthibault.microblog.api.LoginApi;
import com.goubinthibault.microblog.api.dto.Authentification;
import com.goubinthibault.microblog.core.exception.AbstractException;
import com.goubinthibault.microblog.core.login.LoginService;

/**
 * REST controller for LoginApi
 * 
 * @author Thibault GOUBIN
 */
//This class is a REST controller
@Controller
public class LoginController implements LoginApi {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	/**
	 * Service for "login"
	 */
	private LoginService loginService;

	/**
	 * API path
	 */
	public static final String API_PATH = "/login";

	/**
	 * Constructor used by Spring to instanciate other Spring components used in
	 * this controller
	 * 
	 * @param loginService service for "login"
	 */
	public LoginController(LoginService loginService) {
		this.loginService = loginService;
	}

	/**
	 * Implementation of POST /login
	 * 
	 * @param authentification the authentification object
	 * @return the HTTP response (utilisateur or error)
	 */
	@Override
	public ResponseEntity<String> login(Authentification authentification) {
		LOGGER.info("POST {}", API_PATH);

		try {
			// The REST service returns the HTTP response containing the LoginService
			// response with an HTTP status OK
			return new ResponseEntity<String>(
					loginService.authenticate(authentification.getLogin(), authentification.getMotDePasse()),
					HttpStatus.OK);
		} catch (AbstractException e) {
			// The REST service sends an HTTP response containing the error message and the
			// HTTP status corresponding to the exception
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}
}

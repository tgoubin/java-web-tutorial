package com.goubinthibault.microblog.api.utilisateur;

import java.util.ArrayList;
import java.util.List;

import com.goubinthibault.microblog.api.dto.Utilisateur;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurEntity;

/**
 * Mapper for Utilisateur objects
 * 
 * @author Thibault GOUBIN
 */
public class UtilisateurMapper {

	/**
	 * Converts an utilisateur DTO to an utilisateur entity
	 * 
	 * @param dto the utilisateur DTO
	 * @return the corresponding utilisateur entity
	 */
	public static UtilisateurEntity dtoToEntity(Utilisateur dto) {
		UtilisateurEntity entity = new UtilisateurEntity();
		entity.setAbonnements(dtosToEntities(dto.getAbonnements()));
		entity.setAbonnes(dtosToEntities(dto.getAbonnes()));
		entity.setEmail(dto.getEmail());
		entity.setId(dto.getId());
		entity.setLogin(dto.getLogin());
		entity.setNom(dto.getNom());
		entity.setPrenom(dto.getPrenom());
		return entity;
	}

	/**
	 * Converts an utilisateur DTO list to an utilisateur entity list
	 * 
	 * @param dtos the utilisateur DTO list
	 * @return the corresponding utilisateur entity list
	 */
	public static List<UtilisateurEntity> dtosToEntities(List<Utilisateur> dtos) {
		List<UtilisateurEntity> entities = new ArrayList<>();

		if (dtos != null) {
			for (Utilisateur dto : dtos) {
				entities.add(dtoToEntity(dto));
			}
		}

		return entities;
	}

	/**
	 * Converts an utilisateur entity to an utilisateur DTO
	 * 
	 * @param entity the utilisateur entity
	 * @return the corresponding utilisateur DTO
	 */
	public static Utilisateur entityToDto(UtilisateurEntity entity) {
		Utilisateur dto = new Utilisateur();
		dto.setAbonnements(entitiesToDtos(entity.getAbonnements()));
		dto.setAbonnes(entitiesToDtos(entity.getAbonnes()));
		dto.setEmail(entity.getEmail());
		dto.setId(entity.getId());
		dto.setLogin(entity.getLogin());
		dto.setNom(entity.getNom());
		dto.setPrenom(entity.getPrenom());
		return dto;
	}

	/**
	 * Converts an utilisateur entity list to an utilisateur DTO list
	 * 
	 * @param dtos the utilisateur entity list
	 * @return the corresponding utilisateur DTO list
	 */
	public static List<Utilisateur> entitiesToDtos(List<UtilisateurEntity> entities) {
		List<Utilisateur> dtos = new ArrayList<>();

		if (entities != null) {
			for (UtilisateurEntity entity : entities) {
				dtos.add(entityToDto(entity));
			}
		}

		return dtos;
	}
}

package com.goubinthibault.microblog.api.security;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.google.common.net.HttpHeaders;
import com.goubinthibault.microblog.core.security.MicroblogAuthenticationToken;
import com.goubinthibault.microblog.core.security.SecurityConstants;
import com.goubinthibault.microblog.core.security.TokenService;

/**
 * Component for security authentication filter
 * 
 * @author Thibault GOUBIN
 */
// This class is a component
@Component
public class SecurityAuthenticationFilter extends GenericFilterBean {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityAuthenticationFilter.class);

	/**
	 * Token service
	 */
	private TokenService tokenService;

	/**
	 * If the CORS is enabled
	 */
	@Value("${security.cors.enabled}")
	private boolean isCorsActive;

	/**
	 * If the security is enabled
	 */
	@Value("${security.enabled}")
	private boolean isSecurityActive;

	/**
	 * Constructor used by Spring to instanciate other Spring components used in
	 * this service
	 * 
	 * @param tokenService service for "token"
	 */
	public SecurityAuthenticationFilter(TokenService tokenService) {
		this.tokenService = tokenService;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		if ((isCorsActive && HttpMethod.OPTIONS.name().equals(httpRequest.getMethod())) || !isSecurityActive) {
			processAnonymousOrTestAuthentication(httpRequest, httpResponse, chain);
		} else {
			processAuthentication(httpRequest, httpResponse, chain);
		}
	}

	/**
	 * Processes the authentication (for test / anonymous mode)
	 * 
	 * @param httpRequest  the HTTP request
	 * @param httpResponse the HTTP response
	 * @param chain        the filter chain
	 * @throws ServletException a ServletException
	 * @throws IOException      an IOException
	 */
	private void processAnonymousOrTestAuthentication(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			FilterChain chain) throws IOException, ServletException {
		// If security is not active or if "CORS active and OPTIONS call"
		// put a generic authentication to avoid 403 errors
		// Give a user to Spring security

		if (!isSecurityActive) {
			LOGGER.info("Security is not active");
		} else {
			LOGGER.info("CORS active - OPTIONS call {}", httpRequest.getRequestURI());
		}

		setAuthentication(new MicroblogAuthenticationToken("anonymous"));

		// Continues the HTTP request
		chain.doFilter(httpRequest, httpResponse);
	}

	/**
	 * Processes the authentication
	 * 
	 * @param httpRequest
	 * @param httpResponse
	 * @param chain
	 * @throws ServletException a ServletException
	 * @throws IOException      an IOException
	 */
	private void processAuthentication(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			FilterChain chain) throws IOException, ServletException {
		// Authorization headers
		Enumeration<String> headers = httpRequest.getHeaders(HttpHeaders.AUTHORIZATION);

		if (headers == null) {
			// If no header => 401 error
			throwTokenError("No headers are sent", httpResponse);
		} else {
			String token = getTokenFromHeaders(headers);

			if (token == null) {
				// If no Bearer token => 401 error
				throwTokenError("The authentication token does not exist", httpResponse);
			} else {
				validateTokenAndSetAuthentication(httpRequest, httpResponse, chain, token);
			}
		}
	}

	/**
	 * Retrieves the token from the HTTP headers
	 * 
	 * @param headers the headers
	 * @return the token
	 */
	private String getTokenFromHeaders(Enumeration<String> headers) {
		String token = null;

		// Headers loop
		while (headers.hasMoreElements()) {
			String header = headers.nextElement();

			// Token must have a "Bearer" format
			if (header != null && header.startsWith(SecurityConstants.BEARER_PREFIX)) {
				token = header.replaceAll(SecurityConstants.BEARER_PREFIX, "");
			}
		}

		return token;
	}

	/**
	 * Validates the token and sets the authentication in security context
	 * 
	 * @param httpRequest  the HTTP request
	 * @param httpResponse the HTTP response
	 * @param chain        the filter chain
	 * @param token        the token
	 * @throws ServletException a ServletException
	 * @throws IOException      an IOException
	 */
	private void validateTokenAndSetAuthentication(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			FilterChain chain, String token) throws IOException, ServletException {
		LOGGER.debug("Token retrieved : {}", token);

		String userConnected = tokenService.validateToken(token);

		if (userConnected != null) {
			LOGGER.info("The authentication token is valid. The user connected is '{}'", userConnected);
			setAuthentication(new MicroblogAuthenticationToken(userConnected));

			// Continues the HTTP request
			chain.doFilter(httpRequest, httpResponse);
		} else {
			throwTokenError("The authentication token is not valid", httpResponse);
		}
	}

	/**
	 * Sets the authentication in security context
	 * 
	 * @param authentication the authentication object
	 */
	private void setAuthentication(Authentication authentication) {
		SecurityContextHolder.getContext().setAuthentication(authentication);
		MDC.put("intuser", authentication.getPrincipal().toString());
	}

	/**
	 * Throws an Unauthorized error in the HTTP response
	 * 
	 * @param errorMessage the error message
	 * @param httpResponse the HTTP response
	 * @throws IOException an IOException
	 */
	private void throwTokenError(String errorMessage, HttpServletResponse httpResponse) throws IOException {
		LOGGER.error(errorMessage);

		httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
		httpResponse.setContentType(MediaType.TEXT_PLAIN_VALUE);
		httpResponse.getWriter().write(errorMessage);
	}
}

package com.goubinthibault.microblog.api.message.reaction;

import java.util.ArrayList;
import java.util.List;

import com.goubinthibault.microblog.api.dto.ReactionMessage;
import com.goubinthibault.microblog.api.dto.ReactionMessage.ReactionEnum;
import com.goubinthibault.microblog.api.utilisateur.UtilisateurMapper;
import com.goubinthibault.microblog.core.message.MessageEntity;
import com.goubinthibault.microblog.core.message.reaction.ReactionMessageEntity;
import com.goubinthibault.microblog.core.message.reaction.ReactionMessageEntity.ReactionMessageId;
import com.goubinthibault.microblog.core.message.reaction.ReactionMessageEnum;

/**
 * Mapper for ReactionMessage objects
 * 
 * @author Thibault GOUBIN
 */
public class ReactionMessageMapper {

	/**
	 * Converts an reactionMessage DTO to an reactionMessage entity
	 * 
	 * @param dto     the reactionMessage DTO
	 * @param message the message
	 * @return the corresponding reactionMessage entity
	 */
	public static ReactionMessageEntity dtoToEntity(ReactionMessage dto, MessageEntity message) {
		ReactionMessageEntity entity = new ReactionMessageEntity();
		entity.setReaction(ReactionMessageEnum.valueOf(dto.getReaction().name()));
		ReactionMessageId reactionMessageId = new ReactionMessageId();
		reactionMessageId.setMessage(message);
		reactionMessageId.setUtilisateur(UtilisateurMapper.dtoToEntity(dto.getUtilisateur()));
		entity.setId(reactionMessageId);
		return entity;
	}

	/**
	 * Converts an reactionMessage DTO list to an reactionMessage entity list
	 * 
	 * @param dtos    the reactionMessage DTO list
	 * @param message the message
	 * @return the corresponding reactionMessage entity list
	 */
	public static List<ReactionMessageEntity> dtosToEntities(List<ReactionMessage> dtos, MessageEntity message) {
		List<ReactionMessageEntity> entities = new ArrayList<>();

		if (dtos != null) {
			for (ReactionMessage dto : dtos) {
				entities.add(dtoToEntity(dto, message));
			}
		}

		return entities;
	}

	/**
	 * Converts an reactionMessage entity to an reactionMessage DTO
	 * 
	 * @param entity the reactionMessage entity
	 * @return the corresponding reactionMessage DTO
	 */
	public static ReactionMessage entityToDto(ReactionMessageEntity entity) {
		ReactionMessage dto = new ReactionMessage();
		dto.setReaction(ReactionEnum.valueOf(entity.getReaction().name()));
		dto.setUtilisateur(UtilisateurMapper.entityToDto(entity.getId().getUtilisateur()));
		return dto;
	}

	/**
	 * Converts an reactionMessage entity list to an reactionMessage DTO list
	 * 
	 * @param dtos the reactionMessage entity list
	 * @return the corresponding reactionMessage DTO list
	 */
	public static List<ReactionMessage> entitiesToDtos(List<ReactionMessageEntity> entities) {
		List<ReactionMessage> dtos = new ArrayList<>();

		if (entities != null) {
			for (ReactionMessageEntity entity : entities) {
				dtos.add(entityToDto(entity));
			}
		}

		return dtos;
	}
}

package com.goubinthibault.microblog.api.message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.goubinthibault.microblog.api.dto.Message;
import com.goubinthibault.microblog.api.message.reaction.ReactionMessageMapper;
import com.goubinthibault.microblog.api.utilisateur.UtilisateurMapper;
import com.goubinthibault.microblog.core.message.MessageConstants;
import com.goubinthibault.microblog.core.message.MessageEntity;

/**
 * Mapper for Message objects
 * 
 * @author Thibault GOUBIN
 */
public class MessageMapper {

	/**
	 * Converts an message DTO to an message entity
	 * 
	 * @param dto the message DTO
	 * @return the corresponding message entity
	 */
	public static MessageEntity dtoToEntity(Message dto) {
		MessageEntity entity = new MessageEntity();
		entity.setDateCreation(dto.getDateCreation());
		entity.setId(dto.getId());
		entity.setReactionMessages(ReactionMessageMapper.dtosToEntities(dto.getReactions(), entity));
		entity.setTags(
				(dto.getTags() != null) ? StringUtils.join(dto.getTags(), MessageConstants.TAGS_SEPARATOR) : null);
		entity.setTexte(dto.getTexte());

		if (dto.getUtilisateur() != null) {
			entity.setUtilisateur(UtilisateurMapper.dtoToEntity(dto.getUtilisateur()));
		}

		return entity;
	}

	/**
	 * Converts an message DTO list to an message entity list
	 * 
	 * @param dtos the message DTO list
	 * @return the corresponding message entity list
	 */
	public static List<MessageEntity> dtosToEntities(List<Message> dtos) {
		List<MessageEntity> entities = new ArrayList<>();

		if (dtos != null) {
			for (Message dto : dtos) {
				entities.add(dtoToEntity(dto));
			}
		}

		return entities;
	}

	/**
	 * Converts an message entity to an message DTO
	 * 
	 * @param entity the message entity
	 * @return the corresponding message DTO
	 */
	public static Message entityToDto(MessageEntity entity) {
		Message dto = new Message();
		dto.setDateCreation(entity.getDateCreation());
		dto.setId(entity.getId());
		dto.setReactions(ReactionMessageMapper.entitiesToDtos(entity.getReactionMessages()));
		dto.setTags((entity.getTags() != null) ? Arrays.asList(entity.getTags().split(MessageConstants.TAGS_SEPARATOR))
				: null);
		dto.setTexte(entity.getTexte());
		dto.setUtilisateur(UtilisateurMapper.entityToDto(entity.getUtilisateur()));
		return dto;
	}

	/**
	 * Converts an message entity list to an message DTO list
	 * 
	 * @param dtos the message entity list
	 * @return the corresponding message DTO list
	 */
	public static List<Message> entitiesToDtos(List<MessageEntity> entities) {
		List<Message> dtos = new ArrayList<>();

		if (entities != null) {
			for (MessageEntity entity : entities) {
				dtos.add(entityToDto(entity));
			}
		}

		return dtos;
	}
}

DROP TABLE IF EXISTS microblog_reaction_message;
DROP TABLE IF EXISTS microblog_abonnement;
DROP TABLE IF EXISTS microblog_message;
DROP TABLE IF EXISTS microblog_utilisateur;

CREATE TABLE microblog_utilisateur (
	id INT(15) AUTO_INCREMENT NOT NULL,
	login VARCHAR(63) NOT NULL,
	mot_de_passe VARCHAR(63) NOT NULL,
	nom VARCHAR(63) NOT NULL,
	prenom VARCHAR(63) NOT NULL,
	email VARCHAR(63) NOT NULL,
	PRIMARY KEY (id)
) DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE microblog_message (
	id INT(15) AUTO_INCREMENT NOT NULL,
	date_creation DATETIME NOT NULL,
	texte TEXT NOT NULL,
	tags TEXT,
	utilisateur_id INT(15) NOT NULL,
	PRIMARY KEY (id)
) DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

ALTER TABLE microblog_message ADD CONSTRAINT utilisateur_id_message_fk
FOREIGN KEY (utilisateur_id)
REFERENCES microblog_utilisateur (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

CREATE TABLE microblog_abonnement (
	utilisateur_abonne_id INT(15) NOT NULL,
	utilisateur_abonnement_id INT(15) NOT NULL,
	PRIMARY KEY (utilisateur_abonne_id, utilisateur_abonnement_id)
) DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

ALTER TABLE microblog_abonnement ADD CONSTRAINT utilisateur_abonne_id_abonnement_fk
FOREIGN KEY (utilisateur_abonne_id)
REFERENCES microblog_utilisateur (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE microblog_abonnement ADD CONSTRAINT utilisateur_abonnement_id_abonnement_fk
FOREIGN KEY (utilisateur_abonnement_id)
REFERENCES microblog_utilisateur (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

CREATE TABLE microblog_reaction_message (
	utilisateur_id INT(15) NOT NULL,
	message_id INT(15) NOT NULL,
	reaction VARCHAR(15) NOT NULL,
	PRIMARY KEY (utilisateur_id, message_id)
) DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

ALTER TABLE microblog_reaction_message ADD CONSTRAINT utilisateur_id_reaction_message_fk
FOREIGN KEY (utilisateur_id)
REFERENCES microblog_utilisateur (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE microblog_reaction_message ADD CONSTRAINT message_id_reaction_message_fk
FOREIGN KEY (message_id)
REFERENCES microblog_message (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

CREATE INDEX utilisateur_id_message_idx
ON microblog_message (utilisateur_id);

CREATE INDEX utilisateur_abonne_id_abonnement_idx
ON microblog_abonnement (utilisateur_abonne_id);

CREATE INDEX utilisateur_abonnement_id_abonnement_idx
ON microblog_abonnement (utilisateur_abonnement_id);

CREATE INDEX utilisateur_id_reaction_message_idx
ON microblog_reaction_message (utilisateur_id);

CREATE INDEX message_id_reaction_message_idx
ON microblog_reaction_message (message_id);

CREATE INDEX login_utilisateur_idx
ON microblog_utilisateur (login);

CREATE INDEX nom_utilisateur_idx
ON microblog_utilisateur (nom);

CREATE INDEX prenom_utilisateur_idx
ON microblog_utilisateur (prenom);

CREATE UNIQUE INDEX login_utilisateur_unique
ON microblog_utilisateur (login); 

CREATE UNIQUE INDEX email_utilisateur_unique
ON microblog_utilisateur (email); 
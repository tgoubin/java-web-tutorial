package com.goubinthibault.microblog.core.message;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for entity "message"
 * 
 * @author Thibault GOUBIN
 */
// This interface is a repository
// The entity associated is "MessageEntity", with an id represented by an Integer
@Repository
public interface MessageRepository extends CrudRepository<MessageEntity, Integer> {

	/**
	 * Find messages whose author is in a given list
	 * 
	 * @param utilisateurId the list of potential id of authors
	 * @return the messages
	 */
	public List<MessageEntity> findByUtilisateurIdIn(List<Integer> utilisateurId);
}

package com.goubinthibault.microblog.core.utilisateur;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.xml.bind.DatatypeConverter;

import com.goubinthibault.microblog.core.exception.TechnicalException;

/**
 * Utilities for Utilisateur
 * 
 * @author Thibault GOUBIN
 */
public class UtilisateurUtils {

	private static final String MD5 = "MD5";

	/**
	 * Mot de passe encoding (MD5 + Base64)
	 * 
	 * @param motDePasse the mot de passe
	 * @return the mot de passe encoded
	 */
	public static String encodeMotDePasse(String motDePasse) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance(MD5);
			messageDigest.update(motDePasse.getBytes());
			String md5 = DatatypeConverter.printHexBinary(messageDigest.digest()).toUpperCase();
			return Base64.getEncoder().encodeToString(md5.getBytes());
		} catch (NoSuchAlgorithmException e) {
			throw new TechnicalException("Error during mot de passe encoding");
		}
	}
}

package com.goubinthibault.microblog.core.message.reaction;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.goubinthibault.microblog.core.message.MessageEntity;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurEntity;

/**
 * Class for entity "reaction_message"
 * 
 * @author Thibault GOUBIN
 */
//This class is an entity (associated to a database table)
@Entity
//The database table associated is "reaction_message"
@Table(name = "microblog_reaction_message")
public class ReactionMessageEntity implements Serializable {

	private static final long serialVersionUID = 7913339422547516893L;

	/**
	 * Reaction message id
	 */
	// This attribute represents the id of the table, represented by an entire
	// object
	@EmbeddedId
	private ReactionMessageId id;

	/**
	 * Reaction
	 */
	// This attribute represents the "reaction" table column
	// Its value is not nullable (NOT NULL)
	// and the table column value corresponds to a ReactionMessageEnum value
	@Column(name = "reaction", nullable = false)
	@Enumerated(EnumType.STRING)
	private ReactionMessageEnum reaction;

	/**
	 * Class for "reaction_message" id
	 * 
	 * @author Thibault GOUBIN
	 *
	 */
	// This private class is the representation of the ReactionMessageEntity id
	@Embeddable
	public static class ReactionMessageId implements Serializable {

		private static final long serialVersionUID = 4246462290234773390L;

		// This attribute represents the "utilisateur_id" table column
		// but contains the entire "utilisateur" data (not just the id)
		// Its value is not nullable (NOT NULL)
		// "EAGER" specifies that the "UtilisateurEntity" associated
		// is directly attached when the "ReactionMessageEntity" is recovered
		// (contrary to "LAZY", which attaches it on demand)
		@ManyToOne(fetch = FetchType.EAGER)
		@JoinColumn(name = "utilisateur_id", nullable = false)
		private UtilisateurEntity utilisateur;

		// This attribute represents the "message_id" table column
		// but contains the entire "message" data (not just the id)
		// Its value is not nullable (NOT NULL)
		// "EAGER" specifies that the "MessageEntity" associated
		// is directly attached when the "ReactionMessageEntity" is recovered
		// (contrary to "LAZY", which attaches it on demand)
		@ManyToOne(fetch = FetchType.EAGER)
		@JoinColumn(name = "message_id", nullable = false)
		private MessageEntity message;

		public UtilisateurEntity getUtilisateur() {
			return utilisateur;
		}

		public void setUtilisateur(UtilisateurEntity utilisateur) {
			this.utilisateur = utilisateur;
		}

		public MessageEntity getMessage() {
			return message;
		}

		public void setMessage(MessageEntity message) {
			this.message = message;
		}
	}

	public ReactionMessageId getId() {
		return id;
	}

	public void setId(ReactionMessageId id) {
		this.id = id;
	}

	public ReactionMessageEnum getReaction() {
		return reaction;
	}

	public void setReaction(ReactionMessageEnum reaction) {
		this.reaction = reaction;
	}
}

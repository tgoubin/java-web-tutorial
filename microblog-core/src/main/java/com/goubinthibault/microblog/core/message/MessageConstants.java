package com.goubinthibault.microblog.core.message;

/**
 * Constants for Message
 * 
 * @author Thibault GOUBIN
 */
public class MessageConstants {

	/**
	 * Tags separator
	 */
	public static final String TAGS_SEPARATOR = ",";
}

package com.goubinthibault.microblog.core.exception;

/**
 * Abstract class for all exceptions
 * 
 * @author Thibault GOUBIN
 */
public abstract class AbstractException extends RuntimeException {

	private static final long serialVersionUID = -1142519386727484950L;

	/**
	 * The error message
	 */
	private final String errorMessage;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public AbstractException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	/**
	 * HTTP code corresponding to the error
	 * 
	 * @return the HTTP code
	 */
	public abstract int getHttpStatusCode();

	public String getErrorMessage() {
		return errorMessage;
	}
}

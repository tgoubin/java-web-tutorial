package com.goubinthibault.microblog.core.message.reaction;

/**
 * Enumeration for "reaction" values
 * 
 * @author Thibault GOUBIN
 */
public enum ReactionMessageEnum {

	/**
	 * Reactions values list
	 */
	AIME, N_AIME_PAS, ADORE, RIGOLE, TRISTE, EN_COLERE;
}

package com.goubinthibault.microblog.core.utilisateur;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for entity "utilisateur"
 * 
 * @author Thibault GOUBIN
 */
// This interface is a repository
// The entity associated is "UtilisateurEntity", with an id represented by an Integer
@Repository
public interface UtilisateurRepository extends CrudRepository<UtilisateurEntity, Integer> {

	/**
	 * Try to find an utilisateur by a given login
	 * 
	 * @param login the login
	 * @return the optional utilisateur
	 */
	public Optional<UtilisateurEntity> findByLogin(String login);

	/**
	 * Try to find an utilisateur by a given login and not with a given id
	 * 
	 * @param login the login
	 * @param id    the id
	 * @return the optional utilisateur
	 */
	public Optional<UtilisateurEntity> findByLoginAndIdNot(String login, Integer id);

	/**
	 * Try to find an utilisateur by a given email
	 * 
	 * @param login the email
	 * @return the optional utilisateur
	 */
	public Optional<UtilisateurEntity> findByEmail(String email);

	/**
	 * Try to find an utilisateur by a given email and not with a given id
	 * 
	 * @param login the email
	 * @param id    the id
	 * @return the optional utilisateur
	 */
	public Optional<UtilisateurEntity> findByEmailAndIdNot(String email, Integer id);
}

package com.goubinthibault.microblog.core.message;

import java.time.OffsetDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.goubinthibault.microblog.core.common.AbstractEntity;
import com.goubinthibault.microblog.core.message.reaction.ReactionMessageEntity;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurEntity;

/**
 * Class for entity "message"
 * 
 * @author Thibault GOUBIN
 */
// This class is an entity (associated to a database table)
@Entity
// The database table associated is "message"
@Table(name = "microblog_message")
public class MessageEntity extends AbstractEntity {

	private static final long serialVersionUID = 3168216543936323699L;

	/**
	 * Creation date
	 */
	// This attribute represents the "date_creation" table column
	// Its value is not nullable (NOT NULL)
	@Column(name = "date_creation", nullable = false)
	private OffsetDateTime dateCreation;

	/**
	 * Text
	 */
	// This attribute represents the "texte" table column
	// Its value is not nullable (NOT NULL)
	// The format of the "texte" column is "text"
	@Column(name = "texte", columnDefinition = "text", nullable = false)
	private String texte;

	/**
	 * Tags (separated by ',')
	 */
	// This attribute represents the "tags" table column
	// The format of the "tags" column is "text"
	@Column(name = "tags", columnDefinition = "text")
	private String tags;

	/**
	 * Utilisateur
	 */
	// This attribute represents the "utilisateur_id" table column
	// but contains the entire "utilisateur" data (not just the id)
	// Its value is not nullable (NOT NULL)
	// "EAGER" specifies that the "UtilisateurEntity" associated
	// is directly attached when the "MessageEntity" is recovered
	// (contrary to "LAZY", which attaches it on demand)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "utilisateur_id", nullable = false)
	private UtilisateurEntity utilisateur;

	/**
	 * Reactions
	 */
	// This attribute represents the "ReactionMessageEntity" objects linked
	// through the relation between "message" and "reaction_message" tables
	// The mapping on the "ReactionMessageEntity" is provided by the
	// "id.message" attribute
	// "id" is a "ReactionMessageId" instance (complex id for
	// "ReactionMessageEntity")
	@OneToMany(mappedBy = "id.message", cascade = CascadeType.ALL)
	private List<ReactionMessageEntity> reactionMessages;

	public OffsetDateTime getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(OffsetDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getTexte() {
		return texte;
	}

	public void setTexte(String texte) {
		this.texte = texte;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
	}

	public List<ReactionMessageEntity> getReactionMessages() {
		return reactionMessages;
	}

	public void setReactionMessages(List<ReactionMessageEntity> reactionMessages) {
		this.reactionMessages = reactionMessages;
	}
}

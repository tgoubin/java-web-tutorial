package com.goubinthibault.microblog.core.utilisateur;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.goubinthibault.microblog.core.common.AbstractEntity;
import com.goubinthibault.microblog.core.message.MessageEntity;

/**
 * Class for entity "utilisateur"
 * 
 * @author Thibault GOUBIN
 */
//This class is an entity (associated to a database table)
@Entity
//The database table associated is "utilisateur"
@Table(name = "microblog_utilisateur")
public class UtilisateurEntity extends AbstractEntity {

	private static final long serialVersionUID = -5057157907986094192L;

	/**
	 * User login
	 */
	// This attribute represents the "login" table column
	// Its value is not nullable (NOT NULL)
	// Its value is unique
	@Column(name = "login", nullable = false, unique = true)
	private String login;

	/**
	 * User password
	 */
	// This attribute represents the "mot_de_passe" table column
	// Its value is not nullable (NOT NULL)
	@Column(name = "mot_de_passe", nullable = false)
	private String motDePasse;

	/**
	 * User name
	 */
	// This attribute represents the "nom" table column
	// Its value is not nullable (NOT NULL)
	@Column(name = "nom", nullable = false)
	private String nom;

	/**
	 * User first name
	 */
	// This attribute represents the "prenom" table column
	// Its value is not nullable (NOT NULL)
	@Column(name = "prenom", nullable = false)
	private String prenom;

	/**
	 * User email
	 */
	// This attribute represents the "email" table column
	// Its value is not nullable (NOT NULL)
	// Its value is unique
	@Column(name = "email", nullable = false, unique = true)
	private String email;

	/**
	 * User messages
	 */
	// This attribute represents the "MessageEntity" objects linked
	// through the relation between "message" and "utilisateur" tables
	// The mapping on the "MessageEntity" is provided by the
	// "utilisateur" attribute
	@OneToMany(mappedBy = "utilisateur", cascade = CascadeType.ALL)
	private List<MessageEntity> messages;

	/**
	 * Abonnements
	 */
	// This attribute represents the relation "many to many" between
	// two "utilisateur" data, using the "abonnement" association table
	@ManyToMany
	@JoinTable(name = "microblog_abonnement", joinColumns = {
			@JoinColumn(name = "utilisateur_abonne_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "utilisateur_abonnement_id", referencedColumnName = "id") })
	private List<UtilisateurEntity> abonnements;

	/**
	 * Abonnes
	 */
	// This attribute represents the relation "many to many" between
	// two "utilisateur" data, using the "abonnement" association table
	@ManyToMany
	@JoinTable(name = "microblog_abonnement", joinColumns = {
			@JoinColumn(name = "utilisateur_abonnement_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "utilisateur_abonne_id", referencedColumnName = "id") })
	private List<UtilisateurEntity> abonnes;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<MessageEntity> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageEntity> messages) {
		this.messages = messages;
	}

	public List<UtilisateurEntity> getAbonnements() {
		return abonnements;
	}

	public void setAbonnements(List<UtilisateurEntity> abonnements) {
		this.abonnements = abonnements;
	}

	public List<UtilisateurEntity> getAbonnes() {
		return abonnes;
	}

	public void setAbonnes(List<UtilisateurEntity> abonnes) {
		this.abonnes = abonnes;
	}
}

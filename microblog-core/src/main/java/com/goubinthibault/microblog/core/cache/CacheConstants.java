package com.goubinthibault.microblog.core.cache;

/**
 * Constants for cache
 * 
 * @author Thibault GOUBIN
 */
public class CacheConstants {

	/**
	 * Cache messages
	 */
	public static final String CACHE_MESSAGES = "cacheMessages";
}

package com.goubinthibault.microblog.core.spring;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Spring configuration for Microblog
 * 
 * @author Thibault GOUBIN
 */
@Configuration
// The SpringBoot application will manage all components classes in packages "com.goubinthibault.microblog.*"
@ComponentScan({ "com.goubinthibault.microblog" })
// The SpringBoot application will manage all entities in packages "com.goubinthibault.microblog.core.*"
@EntityScan("com.goubinthibault.microblog.core")
// The SpringBoot application enables JPA repositories in packages "com.goubinthibault.microblog.core.*"
@EnableJpaRepositories("com.goubinthibault.microblog.core")
public class MicroblogSpringConfiguration {
}

package com.goubinthibault.microblog.core.utilisateur;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.goubinthibault.microblog.core.common.BusinessUtils;
import com.goubinthibault.microblog.core.exception.ExceptionUtils;

/**
 * Service for "utilisateur"
 * 
 * @author Thibault GOUBIN
 */
// This class is a service
@Service
public class UtilisateurService {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilisateurService.class);

	/**
	 * Repository for "utilisateur" entity
	 */
	private UtilisateurRepository utilisateurRepository;

	/**
	 * Constructor used by Spring to instanciate other Spring components used in
	 * this service
	 * 
	 * @param utilisateurRepository repository for "utilisateur"
	 */
	public UtilisateurService(UtilisateurRepository utilisateurRepository) {
		this.utilisateurRepository = utilisateurRepository;
	}

	/**
	 * Returns the utilisateur connected
	 * 
	 * @return the utilisateur connected
	 */
	public UtilisateurEntity getConnected() {
		return utilisateurRepository
				.findByLogin(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString()).get();
	}

	/**
	 * Returns an utilisateur by a given id
	 * 
	 * @param id the utilisateur id
	 * @return the utilisateur
	 */
	public UtilisateurEntity getById(Integer id) {
		LOGGER.info("UtilisateurService / GetById '{}'", id);

		// Database query
		Optional<UtilisateurEntity> optionalEntity = utilisateurRepository.findById(id);

		// If utilisateur does not exists : Not Found error
		ExceptionUtils.throwNotFoundExceptionIf(!optionalEntity.isPresent(),
				"Utilisateur with id '" + id.toString() + "' does not exist", LOGGER);

		// Security checks - The user connected who try to access the utilisateur data
		// must have the same login
		ExceptionUtils.throwForbiddenExceptionIf(
				!SecurityContextHolder.getContext().getAuthentication().getPrincipal()
						.equals(optionalEntity.get().getLogin()),
				"Utilisateur with login '" + SecurityContextHolder.getContext().getAuthentication().getPrincipal()
						+ "' cannot access Utilisateur with id '" + id.toString() + "'",
				LOGGER);

		return optionalEntity.get();
	}

	/**
	 * Creates an utilisateur
	 * 
	 * @param utilisateur the utilisateur
	 * @return the utilisateur created
	 */
	public UtilisateurEntity create(UtilisateurEntity utilisateur) {
		LOGGER.info("UtilisateurService / Create '{}'", utilisateur.getLogin());

		// Business checks
		createUpdateUtilisateurChecks(utilisateur);

		// Unicity checks
		createUpdateUtilisateurUnicityChecks(null, utilisateur);

		// Fields clean and initialization
		utilisateur.setEmail(utilisateur.getEmail().trim());
		utilisateur.setLogin(utilisateur.getLogin().trim());
		utilisateur.setNom(utilisateur.getNom().trim());
		utilisateur.setPrenom(utilisateur.getPrenom().trim());
		utilisateur.setMotDePasse(UtilisateurUtils.encodeMotDePasse(utilisateur.getLogin()));

		// Database saving and entity saved returning
		return utilisateurRepository.save(utilisateur);
	}

	/**
	 * Updates an utilisateur by a given id
	 * 
	 * @param id          the utilisateur id
	 * @param utilisateur the utilisateur
	 * @return the utilisateur updated
	 */
	public UtilisateurEntity update(Integer id, UtilisateurEntity utilisateur) {
		LOGGER.info("UtilisateurService / Update '{}'", id);

		// Test if utilisateur entity id exists
		// Else Bad Request error
		ExceptionUtils.throwBadRequestExceptionIf((utilisateur.getId() == null), "Utilisateur 'id' is mandatory",
				LOGGER);

		// Test if utilisateur entity id corresponds to id param
		// Else Bad Request error
		ExceptionUtils.throwBadRequestExceptionIf((utilisateur.getId() != id),
				"Utilisateur 'id' does not correspond to 'id' param", LOGGER);

		// Business checks
		createUpdateUtilisateurChecks(utilisateur);

		// Get utilisateur from database
		// If utilisateur does not exists : Not Found error
		UtilisateurEntity utilisateurToUpdate = getById(id);

		// Unicity checks
		createUpdateUtilisateurUnicityChecks(id, utilisateur);

		// Fields updates
		utilisateurToUpdate.setAbonnements(utilisateur.getAbonnements());
		utilisateurToUpdate.setEmail(utilisateur.getEmail().trim());
		utilisateurToUpdate.setLogin(utilisateur.getLogin().trim());
		utilisateurToUpdate.setNom(utilisateur.getNom().trim());
		utilisateurToUpdate.setPrenom(utilisateur.getPrenom().trim());

		// Database saving and entity saved returning
		return utilisateurRepository.save(utilisateurToUpdate);
	}

	/**
	 * Business checks for create / update utilisateur
	 * 
	 * @param utilisateur the utilisateur
	 */
	private void createUpdateUtilisateurChecks(UtilisateurEntity utilisateur) {
		BusinessUtils.checkMandatoryField("login", utilisateur.getLogin(), LOGGER);
		BusinessUtils.checkMandatoryField("nom", utilisateur.getNom(), LOGGER);
		BusinessUtils.checkMandatoryField("prenom", utilisateur.getPrenom(), LOGGER);
		BusinessUtils.checkMandatoryField("email", utilisateur.getEmail(), LOGGER);

		// Login validity
		ExceptionUtils.throwBadRequestExceptionIf((!utilisateur.getLogin().trim().matches("^[a-z0-9._-]+$")),
				"The login '" + utilisateur.getLogin() + "' is not valid", LOGGER);

		// Email validity
		ExceptionUtils.throwBadRequestExceptionIf(
				(!utilisateur.getEmail().trim().matches("^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,}$")),
				"The email '" + utilisateur.getEmail() + "' is not valid", LOGGER);
	}

	/**
	 * Unicity checks for create / update utilisateur
	 * 
	 * @param id          the utilisateur id
	 * @param utilisateur the utilisateur
	 */
	private void createUpdateUtilisateurUnicityChecks(Integer id, UtilisateurEntity utilisateur) {
		String login = utilisateur.getLogin().trim();
		String email = utilisateur.getEmail().trim();

		if (id == null) {
			// Creation case

			// Login unicity
			ExceptionUtils.throwBadRequestExceptionIf((utilisateurRepository.findByLogin(login).isPresent()),
					"The login '" + utilisateur.getLogin() + "' is already used", LOGGER);

			// Email unicity
			ExceptionUtils.throwBadRequestExceptionIf((utilisateurRepository.findByEmail(email).isPresent()),
					"The email '" + utilisateur.getLogin() + "' is already used", LOGGER);
		} else {
			// Update case

			// Login unicity
			ExceptionUtils.throwBadRequestExceptionIf(
					(utilisateurRepository.findByLoginAndIdNot(login, id).isPresent()),
					"The login '" + utilisateur.getLogin() + "' is already used", LOGGER);

			// Email unicity
			ExceptionUtils.throwBadRequestExceptionIf(
					(utilisateurRepository.findByEmailAndIdNot(email, id).isPresent()),
					"The email '" + utilisateur.getLogin() + "' is already used", LOGGER);
		}
	}

	/**
	 * Updates the password of an utilisateur by a given id
	 * 
	 * @param id         the utilisateur id
	 * @param motDePasse the password
	 */
	public void updateMotDePasse(Integer id, String motDePasse) {
		LOGGER.info("UtilisateurService / UpdateMotDePasse '{}'", id);

		// Mot de passe validity :
		// At least one digit [0-9]
		// At least one lowercase character [a-z]
		// At least one uppercase character [A-Z]
		// At least one special character [*.!@#$%^&(){}[]:;<>,.?/~_+-=|\]
		// At least 8 characters in length
		ExceptionUtils.throwBadRequestExceptionIf(
				(!motDePasse
						.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[]:;<>,.?/~_+-=|\\]).{8,}$$")),
				"The password '" + motDePasse + "' is not valid", LOGGER);

		// Get utilisateur from database
		// If utilisateur does not exists : Not Found error
		UtilisateurEntity utilisateurToUpdate = getById(id);

		// Mot de passe update
		utilisateurToUpdate.setMotDePasse(UtilisateurUtils.encodeMotDePasse(motDePasse));

		// Database saving
		utilisateurRepository.save(utilisateurToUpdate);
	}

	/**
	 * Deletes an utilisateur by a given id
	 * 
	 * @param id the utilisateur id
	 */
	public void delete(Integer id) {
		LOGGER.info("UtilisateurService / Delete '{}'", id);

		// Get utilisateur from database
		// If utilisateur does not exists : Not Found error
		UtilisateurEntity utilisateurToDelete = getById(id);

		// Database deleting
		utilisateurRepository.delete(utilisateurToDelete);
	}
}

package com.goubinthibault.microblog.core.cache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.infinispan.AdvancedCache;
import org.infinispan.commons.CacheException;
import org.infinispan.commons.api.BasicCache;
import org.infinispan.spring.common.provider.NullValue;
import org.infinispan.spring.common.provider.SpringCache;
import org.springframework.util.Assert;

/**
 * To manage INVALIDATION_ASYNC mode of infinispan. Avoid PUT command
 * replication in Invalidation mode. Use of nativeCache.putForExternalRead
 * instead of this.nativeCache.put
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class InvalidationCache extends SpringCache {

	private final AdvancedCache<Object, Object> delegate;
	private final long writeTimeout;

	public InvalidationCache(BasicCache nativeCache) {
		this(nativeCache, 0, 0);
	}

	public InvalidationCache(BasicCache nativeCache, long readTimeout, long writeTimeout) {
		super(nativeCache, readTimeout, writeTimeout);
		this.writeTimeout = writeTimeout;
		Assert.isAssignable(AdvancedCache.class, nativeCache.getClass(),
				"An advanced Infinispan cache implementation is required");
		this.delegate = (AdvancedCache) nativeCache;
	}

	@Override
	public void put(final Object key, final Object value) {
		try {
			if (writeTimeout > 0) {
				this.delegate.putAsync(key, value != null ? value : NullValue.NULL).get(writeTimeout,
						TimeUnit.MILLISECONDS);
			} else {
				// FIX - Begin
				this.delegate.putForExternalRead(key, value != null ? value : NullValue.NULL);
				// FIX - End
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new CacheException(e);
		} catch (ExecutionException | TimeoutException e) {
			throw new CacheException(e);
		}
	}

	@Override
	public void put(Object key, Object value, long lifespan, TimeUnit unit) {
		try {
			if (writeTimeout > 0) {
				this.delegate.putAsync(key, value != null ? value : NullValue.NULL, lifespan, unit).get(writeTimeout,
						TimeUnit.MILLISECONDS);
			} else {
				// FIX - Begin
				this.delegate.putForExternalRead(key, value != null ? value : NullValue.NULL, lifespan, unit);
				// FIX - End
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new CacheException(e);
		} catch (ExecutionException | TimeoutException e) {
			throw new CacheException(e);
		}
	}
}

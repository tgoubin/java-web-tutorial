package com.goubinthibault.microblog.core.exception;

import org.springframework.http.HttpStatus;

/**
 * Class for Forbidden exceptions
 * 
 * @author Thibault GOUBIN
 */
public class ForbiddenException extends AbstractException {

	private static final long serialVersionUID = 4339965334589092444L;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public ForbiddenException(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public int getHttpStatusCode() {
		// HTTP code for Unauthorized errors is 403
		return HttpStatus.FORBIDDEN.value();
	}
}

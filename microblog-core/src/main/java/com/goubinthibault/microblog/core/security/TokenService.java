package com.goubinthibault.microblog.core.security;

import java.util.Date;
import java.util.HashMap;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Service for token
 * 
 * @author Thibault GOUBIN
 */
// This class is a service
@Service
public class TokenService {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenService.class);

	/**
	 * Token validity - 5 hours
	 */
	private static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

	/**
	 * Token secret in configuration file
	 */
	@Value("${security.jwt.secret}")
	private String secret;

	/**
	 * Generates token for user
	 * 
	 * @param username the user name
	 * @return the token
	 */
	public String generateToken(String username) {
		return generateToken(username, System.currentTimeMillis(),
				System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000);
	}

	/**
	 * Generates token for user
	 * 
	 * @param username   the user name
	 * @param issuedAt   issue at timestamp
	 * @param expiration expiration timestamp
	 * @return the token
	 */
	public String generateToken(String username, long issuedAt, long expiration) {
		return Jwts.builder().setClaims(new HashMap<>()).setSubject(username).setIssuedAt(new Date(issuedAt))
				.setExpiration(new Date(expiration)).signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	/**
	 * Validates a token
	 * 
	 * @param token the token
	 * @return the username corresponding
	 */
	public String validateToken(String token) {
		try {
			return (!getClaimFromToken(token, Claims::getExpiration).before(new Date()))
					? getClaimFromToken(token, Claims::getSubject)
					: null;
		} catch (Exception e) {
			LOGGER.warn(e.getMessage());
			return null;
		}
	}

	/**
	 * Gets claim from token
	 * 
	 * @param <T>            the function to apply by the claim resolver
	 * @param token          the token
	 * @param claimsResolver the function to apply
	 * @return the claim
	 */
	private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		return claimsResolver.apply(Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody());
	}
}

package com.goubinthibault.microblog.core.exception;

import org.springframework.http.HttpStatus;

/**
 * Class for Unauthorized exceptions
 * 
 * @author Thibault GOUBIN
 */
public class UnauthorizedException extends AbstractException {

	private static final long serialVersionUID = 8710810561791036925L;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public UnauthorizedException(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public int getHttpStatusCode() {
		// HTTP code for Unauthorized errors is 401
		return HttpStatus.UNAUTHORIZED.value();
	}
}

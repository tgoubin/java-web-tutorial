package com.goubinthibault.microblog.core.login;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.goubinthibault.microblog.core.exception.UnauthorizedException;
import com.goubinthibault.microblog.core.security.TokenService;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurEntity;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurRepository;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurUtils;

/**
 * Service for "login"
 * 
 * @author Thibault GOUBIN
 */
// This class is a service
@Service
public class LoginService {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginService.class);

	/**
	 * Service for "token"
	 */
	private TokenService tokenService;

	/**
	 * Repository for "utilisateur" entity
	 */
	private UtilisateurRepository utilisateurRepository;

	/**
	 * Constructor used by Spring to instanciate other Spring components used in
	 * this service
	 * 
	 * @param tokenService          service for "token"
	 * @param utilisateurRepository repository for "utilisateur"
	 */
	public LoginService(TokenService tokenService, UtilisateurRepository utilisateurRepository) {
		this.tokenService = tokenService;
		this.utilisateurRepository = utilisateurRepository;
	}

	/**
	 * Authentication
	 * 
	 * @param login      the login
	 * @param motDePasse the password
	 * @return the authentication token
	 */
	public String authenticate(String login, String motDePasse) {
		LOGGER.info("LoginService / Authenticate '{}'", login);

		// Login / mot de passe checking
		Optional<UtilisateurEntity> utilisateur = utilisateurRepository.findByLogin(login);
		if (!utilisateur.isPresent()) {
			String errorMessage = "Utilisateur with login '" + login + "' does not exist";
			LOGGER.warn(errorMessage);
			throw new UnauthorizedException(errorMessage);
		} else {
			if (!utilisateur.get().getMotDePasse().equals(UtilisateurUtils.encodeMotDePasse(motDePasse))) {
				String errorMessage = "Wrong motDePasse for Utilisateur with login '" + login + "'";
				LOGGER.warn(errorMessage);
				throw new UnauthorizedException(errorMessage);
			}

			// Token generation
			return tokenService.generateToken(login);
		}
	}
}

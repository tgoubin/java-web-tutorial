package com.goubinthibault.microblog.core.exception;

import org.springframework.http.HttpStatus;

/**
 * Class for Bad Request exceptions
 * 
 * @author Thibault GOUBIN
 */
public class BadRequestException extends AbstractException {

	private static final long serialVersionUID = -5530975674604660416L;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public BadRequestException(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public int getHttpStatusCode() {
		// HTTP code for Bad Request errors is 400
		return HttpStatus.BAD_REQUEST.value();
	}
}

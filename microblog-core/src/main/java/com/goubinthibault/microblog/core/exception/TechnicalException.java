package com.goubinthibault.microblog.core.exception;

import org.springframework.http.HttpStatus;

/**
 * Class for Technical exceptions
 * 
 * @author Thibault GOUBIN
 */
public class TechnicalException extends AbstractException {

	private static final long serialVersionUID = 6812501654411238490L;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public TechnicalException(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public int getHttpStatusCode() {
		// HTTP code for Technical errors is 500
		return HttpStatus.INTERNAL_SERVER_ERROR.value();
	}
}

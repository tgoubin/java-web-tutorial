package com.goubinthibault.microblog.core.message;

import java.text.Normalizer;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.goubinthibault.microblog.core.cache.CacheConstants;
import com.goubinthibault.microblog.core.common.BusinessUtils;
import com.goubinthibault.microblog.core.exception.ExceptionUtils;
import com.goubinthibault.microblog.core.message.reaction.ReactionMessageEntity;
import com.goubinthibault.microblog.core.message.reaction.ReactionMessageEntity.ReactionMessageId;
import com.goubinthibault.microblog.core.message.reaction.ReactionMessageEnum;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurEntity;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurService;

/**
 * Service for "message"
 * 
 * @author Thibault GOUBIN
 */
// This class is a service
@Service
public class MessageService {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageService.class);

	/**
	 * Service for "utilisateur"
	 */
	private UtilisateurService utilisateurService;

	/**
	 * Repository for "message" entity
	 */
	private MessageRepository messageRepository;

	/**
	 * Constructor used by Spring to instanciate other Spring components used in
	 * this service
	 * 
	 * @param utilisateurService service for "utilisateur"
	 * @param messageRepository  repository for "message"
	 */
	public MessageService(UtilisateurService utilisateurService, MessageRepository messageRepository) {
		this.utilisateurService = utilisateurService;
		this.messageRepository = messageRepository;
	}

	/**
	 * Returns messages (all, or corresponding to given tags) of the abonnements
	 * 
	 * @param tags the tags
	 * @return the messages
	 */
	@Cacheable(cacheNames = { CacheConstants.CACHE_MESSAGES })
	public List<MessageEntity> get(Set<String> tags) {
		LOGGER.info("MessageService / Get - tags='{}'", tags);

		List<MessageEntity> messages = new ArrayList<>();

		// 1 - All messages of the abonnements + his own login
		UtilisateurEntity utilisateurConnected = utilisateurService.getConnected();
		List<Integer> utilisateursAbonnementsAndOwn = utilisateurConnected.getAbonnements().stream().map(u -> u.getId())
				.collect(Collectors.toList());
		utilisateursAbonnementsAndOwn.add(utilisateurConnected.getId());
		List<MessageEntity> messagesAbonnementsAndOwn = messageRepository
				.findByUtilisateurIdIn(utilisateursAbonnementsAndOwn);

		// 2 - Tags filters
		if (tags != null && !tags.isEmpty()) {
			for (String tag : tags) {
				// For each tag, we keep messagesAbonnements containing it
				messages.addAll(messagesAbonnementsAndOwn.stream()
						.filter(m -> !messages.contains(m) && m.getTags() != null
								&& Arrays.asList(m.getTags().split(MessageConstants.TAGS_SEPARATOR)).contains(tag))
						.collect(Collectors.toList()));
			}
		}

		// Sort by dateCreation, reverse order
		messages.sort(Comparator.comparing(MessageEntity::getDateCreation, Comparator.reverseOrder()));
		return messages;
	}

	/**
	 * Returns a message by a given id
	 * 
	 * @param id the message id
	 * @return the message
	 */
	private MessageEntity getById(Integer id) {
		// Database query
		Optional<MessageEntity> optionalEntity = messageRepository.findById(id);

		// If message does not exists : Not Found error
		ExceptionUtils.throwNotFoundExceptionIf(!optionalEntity.isPresent(),
				"Message with id '" + id.toString() + "' does not exist", LOGGER);

		// Security checks - The user connected who try to access the utilisateur data
		// must have the rights to access the message
		ExceptionUtils.throwForbiddenExceptionIf(
				!SecurityContextHolder.getContext().getAuthentication().getPrincipal()
						.equals(optionalEntity.get().getUtilisateur().getLogin()),
				"Utilisateur with login '" + SecurityContextHolder.getContext().getAuthentication().getPrincipal()
						+ "' cannot access Message with id '" + id.toString() + "'",
				LOGGER);

		return optionalEntity.get();
	}

	/**
	 * Creates a message
	 * 
	 * @param message the message
	 * @return the message created
	 */
	public MessageEntity create(MessageEntity message) {
		LOGGER.info("MessageService / Create '{}'",
				(message.getTexte().length() < 30) ? message.getTexte() : message.getTexte().substring(0, 30) + "...");

		// Business checks
		BusinessUtils.checkMandatoryField("texte", message.getTexte(), LOGGER);

		// Tags retrieving
		message.setTags(findTags(message.getTexte().trim()));

		// Fields clean and initialization
		message.setDateCreation(OffsetDateTime.now());
		message.setTexte(message.getTexte().trim());
		message.setUtilisateur(utilisateurService.getConnected());

		// Database saving and entity saved returning
		return messageRepository.save(message);
	}

	/**
	 * Updates a message by a given id
	 * 
	 * @param id      the message id
	 * @param message the message
	 * @return the message updated
	 */
	public MessageEntity update(Integer id, MessageEntity message) {
		LOGGER.info("MessageService / Update '{}'", id);

		// Test if utilisateur entity id exists
		// Else Bad Request error
		ExceptionUtils.throwBadRequestExceptionIf((message.getId() == null), "Message 'id' is mandatory", LOGGER);

		// Test if utilisateur entity id corresponds to id param
		// Else Bad Request error
		ExceptionUtils.throwBadRequestExceptionIf((message.getId() != id),
				"Message 'id' does not correspond to 'id' param", LOGGER);

		// Business checks
		BusinessUtils.checkMandatoryField("texte", message.getTexte(), LOGGER);

		// Get message from database
		// If message does not exists : Not Found error
		MessageEntity messageToUpdate = getById(id);

		// Tags retrieving
		messageToUpdate.setTags(findTags(message.getTexte().trim()));

		// Fields updates
		messageToUpdate.setTexte(message.getTexte().trim());

		// Database saving and entity saved returning
		return messageRepository.save(messageToUpdate);
	}

	/**
	 * Finds tags from a message texte
	 * 
	 * @param texte the texte
	 * @return the tags
	 */
	private String findTags(String texte) {
		Pattern pattern = Pattern.compile("(#\\w+)(?:#\\w+)*\\b");
		Matcher matcher = pattern.matcher(texte);

		// Tags stored by alphabetic order
		Set<String> tags = new TreeSet<>();
		while (matcher.find()) {
			// Remove accents
			tags.add(Normalizer.normalize(matcher.group(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "")
					.substring(1).toLowerCase());
		}

		return (!tags.isEmpty()) ? StringUtils.join(new ArrayList<>(tags), MessageConstants.TAGS_SEPARATOR) : null;
	}

	/**
	 * Utilisateur connected reaction about a message
	 * 
	 * @param id       the message id
	 * @param reaction the reaction
	 * @return the message updated
	 */
	public MessageEntity react(Integer id, ReactionMessageEnum reaction) {
		LOGGER.info("MessageService / React '{} / {}'", id, reaction.name());

		// Get message from database
		// If message does not exists : Not Found error
		MessageEntity messageToUpdate = getById(id);

		Optional<ReactionMessageEntity> optionalReactionMessage = messageToUpdate.getReactionMessages().stream()
				.filter(r -> r.getId().getUtilisateur().getLogin()
						.equals(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString()))
				.findFirst();
		ReactionMessageEntity reactionMessageToSave = null;

		if (optionalReactionMessage.isPresent()) {
			// If utilisateur already reacted
			reactionMessageToSave = optionalReactionMessage.get();
		} else {
			// Else, if first reaction from the utilisateur
			reactionMessageToSave = new ReactionMessageEntity();
			ReactionMessageId reactionMessageId = new ReactionMessageId();
			reactionMessageId.setMessage(messageToUpdate);
			reactionMessageId.setUtilisateur(utilisateurService.getConnected());
			reactionMessageToSave.setId(reactionMessageId);
			messageToUpdate.getReactionMessages().add(reactionMessageToSave);
		}
		reactionMessageToSave.setReaction(reaction);

		// Database saving and entity saved returning
		return messageRepository.save(messageToUpdate);
	}

	/**
	 * Deletes a message by a given id
	 * 
	 * @param id the message id
	 */
	public void delete(Integer id) {
		LOGGER.info("MessageService / Delete '{}'", id);

		// Get message from database
		// If message does not exists : Not Found error
		MessageEntity messageToDelete = getById(id);

		// Database deleting
		messageRepository.delete(messageToDelete);
	}
}

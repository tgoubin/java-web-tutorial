package com.goubinthibault.microblog.core.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * Class for Microblog authentication token
 * 
 * @author Thibault GOUBIN
 */
public class MicroblogAuthenticationToken extends AbstractAuthenticationToken {

	private static final long serialVersionUID = -784613632890554042L;

	/**
	 * User connected
	 */
	private String userConnected;

	/**
	 * Constructor
	 * 
	 * @param userConnected the user connected
	 */
	public MicroblogAuthenticationToken(String userConnected) {
		super(null);
		this.userConnected = userConnected;
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return userConnected;
	}
}

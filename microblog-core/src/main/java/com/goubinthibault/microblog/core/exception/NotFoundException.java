package com.goubinthibault.microblog.core.exception;

import org.springframework.http.HttpStatus;

/**
 * Class for Not Found exceptions
 * 
 * @author Thibault GOUBIN
 */
public class NotFoundException extends AbstractException {

	private static final long serialVersionUID = 1378184207121277817L;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public NotFoundException(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public int getHttpStatusCode() {
		// HTTP code for Not Found errors is 404
		return HttpStatus.NOT_FOUND.value();
	}
}

package com.goubinthibault.microblog.core.cache;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.configuration.global.ShutdownHookBehavior;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.spring.common.provider.SpringCache;
import org.infinispan.spring.embedded.provider.SpringEmbeddedCacheManager;
import org.infinispan.spring.starter.embedded.InfinispanGlobalConfigurer;
import org.infinispan.transaction.TransactionMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.metrics.cache.CacheMetricsRegistrar;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Caches configuration
 */
@Configuration
//The SpringBoot application will enable the caching
@EnableCaching
public class CacheConfiguration extends CachingConfigurerSupport {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(CacheConfiguration.class);

	@Value("${management.endpoints.jmx.domain:microblog}")
	private String jmxDomain;

	@Value("${cache.server.ip}")
	private String cacheServerIp;

	@Value("${cache.server.port}")
	private String cacheServerPort;

	@Value("#{'${cache.servers.ip}'.split(' ')}")
	private List<String> cacheServersIp;

	/**
	 * Cache reset
	 */
	@CacheEvict(cacheNames = { CacheConstants.CACHE_MESSAGES })
	@Scheduled(fixedDelay = 60000)
	public void cacheEvict() {
		LOGGER.info("'{}' cache reset", CacheConstants.CACHE_MESSAGES);
	}

	/**
	 * Global cache configuration
	 * 
	 * @return global cache configuration
	 */
	@Bean
	public InfinispanGlobalConfigurer globalConfigurer() {
		// Properties definition to inject them in infinispan-jgroups.xml
		System.setProperty("cache.server.ip", cacheServerIp);
		System.setProperty("cache.server.port", cacheServerPort);
		String hosts = cacheServersIp.stream().map(ip -> ip + "[" + cacheServerPort + "]")
				.collect(Collectors.joining(","));
		System.setProperty("cache.servers.ip", hosts);

		return () -> {
			GlobalConfigurationBuilder builder = new GlobalConfigurationBuilder();
			builder.jmx().enable().domain(jmxDomain).transport().defaultTransport()
					.clusterName(jmxDomain + "_cache_cluster") // .nodeName("MY_NODE")
					.addProperty("configurationFile", "infinispan-jgroups.xml").shutdown()
					.hookBehavior(ShutdownHookBehavior.DONT_REGISTER).serialization().whiteList().addRegexp(".*");
			return builder.build();
		};
	}

	/**
	 * Application caches declarator
	 */
	@Component
	private static class CacheDeclarator {

		@Autowired
		private CacheMetricsRegistrar cacheMetricsRegistrar;

		@Autowired
		private EmbeddedCacheManager infinispanCacheManager;

		@Autowired
		private CacheManager springCacheManager;

		/**
		 * Application caches declaration
		 */
		@PostConstruct
		public void declareCaches() {
			LOGGER.info("'{}' cache configuration ", CacheConstants.CACHE_MESSAGES);
			infinispanCacheManager.defineConfiguration(CacheConstants.CACHE_MESSAGES, templateReplicatedCache());
			cacheMetricsRegistrar.bindCacheToRegistry(springCacheManager.getCache(CacheConstants.CACHE_MESSAGES));
		}

		/**
		 * Clean cache stopping
		 */
		@PreDestroy
		public void stopCaches() {
			LOGGER.info("Cache manager stopping");
			((SpringEmbeddedCacheManager) springCacheManager).stop();
		}

		/**
		 * Cache replication model between same cluster servers (via invalidation)
		 * 
		 * @return the configuration
		 */
		public org.infinispan.configuration.cache.Configuration templateReplicatedCache() {
			return new ConfigurationBuilder().clustering().cacheMode(CacheMode.INVALIDATION_ASYNC).statistics().enable()
					.memory().size(50000L).memory().evictionType(EvictionType.COUNT).transaction()
					.transactionMode(TransactionMode.NON_TRANSACTIONAL).locking().concurrencyLevel(64)
					.lockAcquisitionTimeout(30, TimeUnit.SECONDS).build();

		}
	}

	/**
	 * Cache key generator
	 */
	@Override
	public KeyGenerator keyGenerator() {
		return (target, method, params) -> target.getClass().getSimpleName() + "." + method.getName() + "("
				+ org.springframework.util.StringUtils.arrayToDelimitedString(params, "_") + ")";
	}

	/**
	 * Cache customisation to avoid PUT command replication in Invalidation mode.
	 * Use of nativeCache.putForExternalRead instead of this.nativeCache.put
	 */
	@Bean
	public SpringEmbeddedCacheManager springEmbeddedCacheManager(EmbeddedCacheManager embeddedCacheManager) {
		return new SpringEmbeddedCacheManager(embeddedCacheManager) {
			private final ConcurrentMap<String, SpringCache> springCaches = new ConcurrentHashMap<>();

			@Override
			public SpringCache getCache(String name) {
				return springCaches.computeIfAbsent(name, n -> {
					return new InvalidationCache(this.getNativeCacheManager().getCache(n));
				});
			}

			@Override
			public void stop() {
				super.stop();
				springCaches.clear();
			}
		};
	}
}

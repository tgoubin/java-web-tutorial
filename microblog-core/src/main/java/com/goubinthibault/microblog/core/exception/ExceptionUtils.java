package com.goubinthibault.microblog.core.exception;

import org.slf4j.Logger;

/**
 * Utilities for exception
 * 
 * @author Thibault GOUBIN
 */
public class ExceptionUtils {

	/**
	 * Throws a BadRequestException if a condition is checked
	 * 
	 * @param badRequestCondition the BadRequest confition
	 * @param errorMessage        the error message
	 * @param logger              the logger
	 */
	public static void throwBadRequestExceptionIf(boolean badRequestCondition, String errorMessage, Logger logger) {
		if (badRequestCondition) {
			logger.warn(errorMessage);
			throw new BadRequestException(errorMessage);
		}
	}

	/**
	 * Throws a ForbiddenException if a condition is checked
	 * 
	 * @param forbiddenCondition the Forbidden confition
	 * @param errorMessage       the error message
	 * @param logger             the logger
	 */
	public static void throwForbiddenExceptionIf(boolean forbiddenCondition, String errorMessage, Logger logger) {
		if (forbiddenCondition) {
			logger.warn(errorMessage);
			throw new ForbiddenException(errorMessage);
		}
	}

	/**
	 * Throws a NotFoundException if a condition is checked
	 * 
	 * @param notFoundCondition the NotFound confition
	 * @param errorMessage      the error message
	 * @param logger            the logger
	 */
	public static void throwNotFoundExceptionIf(boolean notFoundCondition, String errorMessage, Logger logger) {
		if (notFoundCondition) {
			logger.warn(errorMessage);
			throw new NotFoundException(errorMessage);
		}
	}
}

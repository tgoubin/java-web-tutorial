package com.goubinthibault.microblog.core.security;

/**
 * Constants for security
 * 
 * @author Thibault GOUBIN
 */
public class SecurityConstants {

	/**
	 * Bearer prefix
	 */
	public static final String BEARER_PREFIX = "Bearer ";
}

package com.goubinthibault.microblog.core.common;

import org.slf4j.Logger;

import com.goubinthibault.microblog.core.exception.ExceptionUtils;

/**
 * Utilities for business processes
 * 
 * @author Thibault GOUBIN
 */
public class BusinessUtils {

	/**
	 * Mandatory field check
	 * 
	 * @param fieldName  the field name
	 * @param fieldValue the field value
	 */
	public static void checkMandatoryField(String fieldName, String fieldValue, Logger logger) {
		ExceptionUtils.throwBadRequestExceptionIf((fieldValue == null || "".equals(fieldValue.trim())),
				"The '" + fieldName + "' field is mandatory", logger);
	}
}

package com.goubinthibault.microblog.tests.api.message;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.jdbc.Sql;

import com.goubinthibault.microblog.api.dto.Message;
import com.goubinthibault.microblog.tests.common.MessageFactory;
import com.goubinthibault.microblog.tests.common.TestConstants;

/**
 * POST Message API tests
 * 
 * @author Thibault GOUBIN
 */
public class PostMessageApiTest extends MessageApiTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PostMessageApiTest.class);

	private static final String REQUESTS_FOLDER = MessageApiTestConstants.POST_MESSAGE_REQUESTS_FOLDER
			+ "postMessageApiTest/";

	/**
	 * POST /message - 201
	 * 
	 * @throws Exception an Exception
	 */
	@Test
	@Sql(TestConstants.RESET_SQL_DATA_FILE)
	public void postMessage_201() throws Exception {
		LOGGER.info("POST /message - 201");

		// Message expected definition
		Message message = MessageFactory.createMessage(OffsetDateTime.now(), 6, new ArrayList<>(),
				Arrays.asList("tags", "test", "unique"), "Message #test avec des #tags et un #unique",
				MessageApiTestConstants.MESSAGE_API_TEST_POST_UTILISATEUR_INIT_DATA_1);

		// POST /message call and response check
		MessageApiTestUtils.postMessageOK(mockMvc, resourceLoader, REQUESTS_FOLDER + "postMessage_201", message);

		// GET /message?tag=unique call and response check
		MessageApiTestUtils.getMessagesOK(mockMvc, Arrays.asList("unique"), Arrays.asList(message));
	}
}

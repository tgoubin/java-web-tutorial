package com.goubinthibault.microblog.tests.api.message;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.jdbc.Sql;

import com.goubinthibault.microblog.tests.common.TestConstants;

/**
 * GET Message API tests
 * 
 * @author Thibault GOUBIN
 */
public class GetMessagesApiTest extends MessageApiTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(GetMessagesApiTest.class);

	/**
	 * GET /message - 200 - Scenario 1
	 * 
	 * @throws Exception an Exception
	 */
	@Test
	@Sql(TestConstants.RESET_SQL_DATA_FILE)
	public void getMessages_200_1() throws Exception {
		LOGGER.info("GET /message - 200 - Scenario 1");

		// GET /message call and response check
		MessageApiTestUtils.getMessagesOK(mockMvc, Arrays.asList("tags"),
				Arrays.asList(MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_5,
						MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_3));
	}

	/**
	 * GET /message - 200 - Scenario 2
	 * 
	 * @throws Exception an Exception
	 */
	@Test
	@Sql(TestConstants.RESET_SQL_DATA_FILE)
	public void getMessages_200_2() throws Exception {
		LOGGER.info("GET /message - 200 - Scenario 2");

		// GET /message call and response check
		MessageApiTestUtils.getMessagesOK(mockMvc, Arrays.asList("test"),
				Arrays.asList(MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_4,
						MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_3));
	}

	/**
	 * GET /message - 200 - Scenario 3
	 * 
	 * @throws Exception an Exception
	 */
	@Test
	@Sql(TestConstants.RESET_SQL_DATA_FILE)
	public void getMessages_200_3() throws Exception {
		LOGGER.info("GET /message - 200 - Scenario 3");

		// GET /message call and response check
		MessageApiTestUtils.getMessagesOK(mockMvc, Arrays.asList("message"),
				Arrays.asList(MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_5));
	}

	/**
	 * GET /message - 200 - Scenario 4
	 * 
	 * @throws Exception an Exception
	 */
	@Test
	@Sql(TestConstants.RESET_SQL_DATA_FILE)
	public void getMessages_200_4() throws Exception {
		LOGGER.info("GET /message - 200 - Scenario 4");

		// GET /message call and response check
		MessageApiTestUtils.getMessagesOK(mockMvc, Arrays.asList("test", "message"),
				Arrays.asList(MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_5,
						MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_4,
						MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_3));
	}
}

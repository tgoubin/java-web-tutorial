package com.goubinthibault.microblog.tests.api.message;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.jdbc.Sql;

import com.goubinthibault.microblog.api.dto.ReactionMessage.ReactionEnum;
import com.goubinthibault.microblog.tests.common.ReactionMessageFactory;
import com.goubinthibault.microblog.tests.common.TestConstants;

/**
 * PUT Message/Reaction API tests
 * 
 * @author Thibault GOUBIN
 */
public class PutReactionMessageApiTest extends MessageApiTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PutReactionMessageApiTest.class);

	private static final String REQUESTS_FOLDER = MessageApiTestConstants.PUT_REACTION_MESSAGE_REQUESTS_FOLDER
			+ "putReactionMessageApiTest/";

	/**
	 * PUT /message/{idMessage}/reaction - 200
	 * 
	 * @throws Exception an Exception
	 */
	@Test
	@Sql(TestConstants.RESET_SQL_DATA_FILE)
	public void putReactionMessage_200() throws Exception {
		LOGGER.info("PUT /message/{idMessage}/reaction - 200");

		// FIRST REACTION

		// Message expected definition
		MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_2
				.setReactions(Arrays.asList(ReactionMessageFactory.createReactionMessage(ReactionEnum.ADORE,
						MessageApiTestConstants.MESSAGE_API_TEST_POST_UTILISATEUR_INIT_DATA_1)));

		// PUT /message/{idMessage}/reaction call and response check
		MessageApiTestUtils.putReactionMessageOK(mockMvc, resourceLoader,
				MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_2.getId(),
				REQUESTS_FOLDER + "putReactionMessage_200_1",
				MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_2);

		// GET /message?tag=2 call and response check
		MessageApiTestUtils.getMessagesOK(mockMvc, Arrays.asList("2"),
				Arrays.asList(MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_2));

		// CACHE RESET (for MessageService::get(...) call, which uses cache)
		resetCacheMessages();

		// SECOND REACTION FROM THE SAME USER

		// Message expected definition
		MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_2
				.setReactions(Arrays.asList(ReactionMessageFactory.createReactionMessage(ReactionEnum.EN_COLERE,
						MessageApiTestConstants.MESSAGE_API_TEST_POST_UTILISATEUR_INIT_DATA_1)));

		// PUT /message/{idMessage}/reaction call and response check
		MessageApiTestUtils.putReactionMessageOK(mockMvc, resourceLoader,
				MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_2.getId(),
				REQUESTS_FOLDER + "putReactionMessage_200_2",
				MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_2);

		// GET /message?tag=2 call and response check
		MessageApiTestUtils.getMessagesOK(mockMvc, Arrays.asList("2"),
				Arrays.asList(MessageApiTestConstants.MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_2));
	}
}

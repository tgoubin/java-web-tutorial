package com.goubinthibault.microblog.tests.api.login;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.goubinthibault.microblog.api.login.LoginController;
import com.goubinthibault.microblog.core.security.TokenService;
import com.goubinthibault.microblog.tests.common.TestConstants;
import com.goubinthibault.microblog.tests.common.api.ApiTestUtils;

/**
 * POST Login API tests
 * 
 * @author Thibault GOUBIN
 */
public class PostLoginApiTest extends LoginApiTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PostLoginApiTest.class);

	/**
	 * Service for "token"
	 */
	@Autowired
	private TokenService tokenService;

	private static final String REQUESTS_FOLDER = LoginApiTestConstants.POST_LOGIN_REQUESTS_FOLDER
			+ "postLoginApiTest/";

	/**
	 * POST /login - 200
	 * 
	 * @throws Exception an Exception
	 */
	@Test
	@Sql(TestConstants.RESET_SQL_DATA_FILE)
	public void postLogin_200() throws Exception {
		LOGGER.info("POST /login - 200");

		// POST /login call and response check
		LoginApiTestUtils.postLoginOK(mockMvc, resourceLoader, REQUESTS_FOLDER + "postLogin_200", tokenService, "test");
	}

	/**
	 * POST /login - 401
	 * 
	 * @throws Exception an Exception
	 */
	@Test
	@Sql(TestConstants.RESET_SQL_DATA_FILE)
	public void postLogin_401() throws Exception {
		LOGGER.info("POST /login - 401");

		// POST /login call and response check
		ResultActions resultActions = ApiTestUtils.post(mockMvc, LoginController.API_PATH,
				ApiTestUtils.getRequestFileContent(resourceLoader, REQUESTS_FOLDER + "postLogin_401"));
		resultActions.andExpect(MockMvcResultMatchers.status().isUnauthorized());
	}
}

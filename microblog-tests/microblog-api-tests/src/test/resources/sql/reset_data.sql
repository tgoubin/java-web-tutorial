DELETE FROM microblog_utilisateur;
ALTER TABLE microblog_utilisateur AUTO_INCREMENT = 1;
DELETE FROM microblog_message;
ALTER TABLE microblog_message AUTO_INCREMENT = 1;
DELETE FROM microblog_abonnement;
DELETE FROM microblog_reaction_message;
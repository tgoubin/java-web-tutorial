package com.goubinthibault.microblog.tests.api.login;

import com.goubinthibault.microblog.api.login.LoginController;
import com.goubinthibault.microblog.tests.common.api.ApiTestConstants;

/**
 * Login API tests constants
 * 
 * @author Thibault GOUBIN
 */
public class LoginApiTestConstants {

	/**
	 * POST /login requests data folder: "src/test/resources/requests/login/post"
	 */
	public static final String POST_LOGIN_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER + LoginController.API_PATH
			+ "/post/";
}

package com.goubinthibault.microblog.tests.api.utilisateur;

import com.goubinthibault.microblog.api.utilisateur.UtilisateurController;
import com.goubinthibault.microblog.tests.common.api.ApiTestConstants;

/**
 * Utilisateur API tests constants
 * 
 * @author Thibault GOUBIN
 */
public class UtilisateurApiTestConstants {

	/**
	 * POST /utilisateur requests data folder:
	 * "src/test/resources/requests/utilisateur/post"
	 */
	public static final String POST_UTILISATEUR_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER
			+ UtilisateurController.API_PATH + "/post/";

	/**
	 * PUT /utilisateur/{idUtilisateur} requests data folder
	 * "src/test/resources/requests/utilisateur/put"
	 */
	public static final String PUT_UTILISATEUR_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER
			+ UtilisateurController.API_PATH + "/put/";
}

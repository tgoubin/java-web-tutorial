package com.goubinthibault.microblog.tests.api.login;

import com.goubinthibault.microblog.api.utilisateur.UtilisateurController;
import com.goubinthibault.microblog.tests.api.utilisateur.UtilisateurApiTestConstants;
import com.goubinthibault.microblog.tests.common.api.ApiTest;
import com.goubinthibault.microblog.tests.common.api.ApiTestUtils;

/**
 * Abstract Login API tests class
 * 
 * @author Thibault GOUBIN
 */
public abstract class LoginApiTest extends ApiTest {

	/**
	 * LoginApiTest requests data folder:
	 * "src/test/resources/requests/utilisateur/post/loginApiTest"
	 */
	private static final String INIT_DATA_REQUESTS_FOLDER = UtilisateurApiTestConstants.POST_UTILISATEUR_REQUESTS_FOLDER
			+ "loginApiTest/";

	@Override
	public void initData() throws Exception {
		ApiTestUtils.post(mockMvc, UtilisateurController.API_PATH,
				ApiTestUtils.getRequestFileContent(resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_1"));
		ApiTestUtils.post(mockMvc, UtilisateurController.API_PATH,
				ApiTestUtils.getRequestFileContent(resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_2"));
	}
}

package com.goubinthibault.microblog.tests.api.message;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import com.goubinthibault.microblog.api.dto.Message;
import com.goubinthibault.microblog.api.dto.Utilisateur;
import com.goubinthibault.microblog.api.message.MessageController;
import com.goubinthibault.microblog.tests.common.MessageFactory;
import com.goubinthibault.microblog.tests.common.UtilisateurFactory;
import com.goubinthibault.microblog.tests.common.api.ApiTestConstants;

/**
 * Message API tests constants
 * 
 * @author Thibault GOUBIN
 */
public class MessageApiTestConstants {

	/**
	 * POST /message requests data folder:
	 * "src/test/resources/requests/message/post"
	 */
	public static final String POST_MESSAGE_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER
			+ MessageController.API_PATH + "/post/";

	/**
	 * PUT /message/{idMessage}/reaction requests data folder:
	 * "src/test/resources/requests/message/put_reaction"
	 */
	public static final String PUT_REACTION_MESSAGE_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER
			+ MessageController.API_PATH + "/put_reaction/";

	/**
	 * Utilisateur defined in
	 * "src/test/resources/requests/utilisateur/post/messageApiTest/initData_1.json"
	 */
	public static final Utilisateur MESSAGE_API_TEST_POST_UTILISATEUR_INIT_DATA_1 = UtilisateurFactory
			.createUtilisateurAnonymous();

	/**
	 * Message defined in
	 * "src/test/resources/requests/message/post/messageApiTest/initData_2.json"
	 */
	public static final Message MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_2 = MessageFactory.createMessage(
			OffsetDateTime.now(), 2, new ArrayList<>(), Arrays.asList("2"), "Message test #2",
			MESSAGE_API_TEST_POST_UTILISATEUR_INIT_DATA_1);

	/**
	 * Message defined in
	 * "src/test/resources/requests/message/post/messageApiTest/initData_3.json"
	 */
	public static final Message MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_3 = MessageFactory.createMessage(
			OffsetDateTime.now(), 3, new ArrayList<>(), Arrays.asList("tags", "test"), "Message #test 3 avec des #tags",
			MESSAGE_API_TEST_POST_UTILISATEUR_INIT_DATA_1);

	/**
	 * Message defined in
	 * "src/test/resources/requests/message/post/messageApiTest/initData_4.json"
	 */
	public static final Message MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_4 = MessageFactory.createMessage(
			OffsetDateTime.now(), 4, new ArrayList<>(), Arrays.asList("test"), "Message #test 4 avec un tag",
			MESSAGE_API_TEST_POST_UTILISATEUR_INIT_DATA_1);

	/**
	 * Message defined in
	 * "src/test/resources/requests/message/post/messageApiTest/initData_5.json"
	 */
	public static final Message MESSAGE_API_TEST_POST_MESSAGE_INIT_DATA_5 = MessageFactory.createMessage(
			OffsetDateTime.now(), 5, new ArrayList<>(), Arrays.asList("message", "tags"),
			"#Message test 5 avec des #tags", MESSAGE_API_TEST_POST_UTILISATEUR_INIT_DATA_1);
}

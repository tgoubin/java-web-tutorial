package com.goubinthibault.microblog.tests.api.login;

import org.junit.jupiter.api.Assertions;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.goubinthibault.microblog.api.login.LoginController;
import com.goubinthibault.microblog.core.security.TokenService;
import com.goubinthibault.microblog.tests.common.api.ApiTestUtils;

/**
 * Login API tests utilities
 * 
 * @author Thibault GOUBIN
 */
public class LoginApiTestUtils {

	/**
	 * POST /login execution and check request execution success
	 * 
	 * @param mockMvc        the mock MVC
	 * @param resourceLoader the resource loader
	 * @param requestFile    the JSON request file
	 * @param tokenService   the service for "token"
	 * @param login          the expected login
	 * @throws Exception an exception
	 */
	public static void postLoginOK(MockMvc mockMvc, ResourceLoader resourceLoader, String requestFile,
			TokenService tokenService, String login) throws Exception {
		ResultActions resultActions = ApiTestUtils
				.post(mockMvc, LoginController.API_PATH,
						ApiTestUtils.getRequestFileContent(resourceLoader, requestFile))
				.andExpect(MockMvcResultMatchers.status().isOk());
		String token = resultActions.andReturn().getResponse().getContentAsString();
		Assertions.assertEquals(login, tokenService.validateToken(token));
	}
}

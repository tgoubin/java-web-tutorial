package com.goubinthibault.microblog.tests.api.message;

import org.infinispan.manager.EmbeddedCacheManager;
import org.springframework.beans.factory.annotation.Autowired;

import com.goubinthibault.microblog.api.message.MessageController;
import com.goubinthibault.microblog.api.utilisateur.UtilisateurController;
import com.goubinthibault.microblog.core.cache.CacheConstants;
import com.goubinthibault.microblog.tests.api.utilisateur.UtilisateurApiTestConstants;
import com.goubinthibault.microblog.tests.common.api.ApiTest;
import com.goubinthibault.microblog.tests.common.api.ApiTestUtils;

/**
 * Abstract Message API tests class
 * 
 * @author Thibault GOUBIN
 */
public abstract class MessageApiTest extends ApiTest {

	private static final String MESSAGE_API_TEST_REQUESTS = "messageApiTest/";

	/**
	 * Cache manager
	 */
	@Autowired
	private EmbeddedCacheManager infinispanCacheManager;

	/**
	 * MessageApiTest requests data folder:
	 * "src/test/resources/requests/utilisateur/post/messageApiTest"
	 */
	private static final String INIT_UTILISATEUR_DATA_REQUESTS_FOLDER = UtilisateurApiTestConstants.POST_UTILISATEUR_REQUESTS_FOLDER
			+ MESSAGE_API_TEST_REQUESTS;

	/**
	 * "src/test/resources/requests/message/post/messageApiTest"
	 */
	private static final String INIT_MESSAGE_DATA_REQUESTS_FOLDER = MessageApiTestConstants.POST_MESSAGE_REQUESTS_FOLDER
			+ MESSAGE_API_TEST_REQUESTS;

	@Override
	public void initData() throws Exception {
		ApiTestUtils.post(mockMvc, UtilisateurController.API_PATH, ApiTestUtils.getRequestFileContent(resourceLoader,
				INIT_UTILISATEUR_DATA_REQUESTS_FOLDER + "initData_1"));
		ApiTestUtils.post(mockMvc, MessageController.API_PATH,
				ApiTestUtils.getRequestFileContent(resourceLoader, INIT_MESSAGE_DATA_REQUESTS_FOLDER + "initData_1"));
		Thread.sleep(1500);
		ApiTestUtils.post(mockMvc, MessageController.API_PATH,
				ApiTestUtils.getRequestFileContent(resourceLoader, INIT_MESSAGE_DATA_REQUESTS_FOLDER + "initData_2"));
		Thread.sleep(1500);
		ApiTestUtils.post(mockMvc, MessageController.API_PATH,
				ApiTestUtils.getRequestFileContent(resourceLoader, INIT_MESSAGE_DATA_REQUESTS_FOLDER + "initData_3"));
		Thread.sleep(1500);
		ApiTestUtils.post(mockMvc, MessageController.API_PATH,
				ApiTestUtils.getRequestFileContent(resourceLoader, INIT_MESSAGE_DATA_REQUESTS_FOLDER + "initData_4"));
		Thread.sleep(1500);
		ApiTestUtils.post(mockMvc, MessageController.API_PATH,
				ApiTestUtils.getRequestFileContent(resourceLoader, INIT_MESSAGE_DATA_REQUESTS_FOLDER + "initData_5"));
		Thread.sleep(1500);
	}

	/**
	 * Cache reset
	 */
	public void resetCacheMessages() {
		infinispanCacheManager.getCache(CacheConstants.CACHE_MESSAGES).clear();
	}
}

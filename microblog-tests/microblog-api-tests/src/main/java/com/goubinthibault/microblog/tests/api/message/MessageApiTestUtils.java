package com.goubinthibault.microblog.tests.api.message;

import java.time.OffsetDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.goubinthibault.microblog.api.dto.Message;
import com.goubinthibault.microblog.api.message.MessageController;
import com.goubinthibault.microblog.core.message.MessageConstants;
import com.goubinthibault.microblog.tests.common.api.ApiTestConstants;
import com.goubinthibault.microblog.tests.common.api.ApiTestUtils;

/**
 * Message API tests utilities
 * 
 * @author Thibault GOUBIN
 */
public class MessageApiTestUtils {

	/**
	 * GET /message?tags={tags} execution and check request execution success
	 * 
	 * @param mockMvc  the mock MVC
	 * @param tags     the tags
	 * @param messages the messages expected
	 * @throws Exception an Exception
	 */
	public static void getMessagesOK(MockMvc mockMvc, List<String> tags, List<Message> messages) throws Exception {
		ResultActions resultActions = ApiTestUtils.get(mockMvc,
				MessageController.API_PATH + "?tag=" + StringUtils.join(tags, MessageConstants.TAGS_SEPARATOR), null);
		resultActions.andExpect(MockMvcResultMatchers.status().isOk());

		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(messages.size())));
		for (int i = 0; i < messages.size(); i++) {
			verifyMessageContent(resultActions, messages.get(i), i);
		}
	}

	/**
	 * POST /message execution and check request execution success
	 * 
	 * @param mockMvc        the mock MVC
	 * @param resourceLoader the resource loader
	 * @param requestFile    the JSON request file
	 * @param message        the expected message
	 * @throws Exception an Exception
	 */
	public static void postMessageOK(MockMvc mockMvc, ResourceLoader resourceLoader, String requestFile,
			Message message) throws Exception {
		ResultActions resultActions = ApiTestUtils.post(mockMvc, MessageController.API_PATH,
				ApiTestUtils.getRequestFileContent(resourceLoader, requestFile));
		resultActions.andExpect(MockMvcResultMatchers.status().isCreated());
		verifyMessageContent(resultActions, message, null);
	}

	/**
	 * PUT /message/{id} execution and check request execution success
	 * 
	 * @param mockMvc        the mock MVC
	 * @param resourceLoader the resource loader
	 * @param id             the message id
	 * @param requestFile    the JSON request file
	 * @param message        the message expected
	 * @throws Exception an exception
	 */
	public static void putReactionMessageOK(MockMvc mockMvc, ResourceLoader resourceLoader, Integer id,
			String requestFile, Message message) throws Exception {
		ResultActions resultActions = ApiTestUtils.put(mockMvc,
				MessageController.API_PATH + "/" + String.valueOf(id) + "/reaction",
				ApiTestUtils.getRequestFileContent(resourceLoader, requestFile));
		resultActions.andExpect(MockMvcResultMatchers.status().isOk());
		verifyMessageContent(resultActions, message, null);
	}

	/**
	 * Verifies the HTTP response content from a message object expected
	 * 
	 * @param resultActions the result actions
	 * @param message       the expected message
	 * @param index         the index (optional) of the message in the HTTP response
	 * @throws Exception an Exception
	 */
	private static void verifyMessageContent(ResultActions resultActions, Message message, Integer index)
			throws Exception {
		String jsonPath = "$";
		if (index != null) {
			jsonPath += "[" + index + "]";
		}

		resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".id").value(message.getId()));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".reactions").isArray());
		resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".texte").value(message.getTexte()));
		resultActions.andExpect(
				MockMvcResultMatchers.jsonPath(jsonPath + ".utilisateur.id").value(message.getUtilisateur().getId()));

		resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".reactions",
				Matchers.hasSize(message.getReactions().size())));
		for (int i = 0; i < message.getReactions().size(); i++) {
			resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".reactions[" + i + "].reaction")
					.value(message.getReactions().get(i).getReaction().name()));
			resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".reactions[" + i + "].utilisateur.id")
					.value(message.getReactions().get(i).getUtilisateur().getId()));
		}

		if (message.getTags() != null) {
			resultActions.andExpect(
					MockMvcResultMatchers.jsonPath(jsonPath + ".tags", Matchers.hasSize(message.getTags().size())));
			for (int j = 0; j < message.getTags().size(); j++) {
				resultActions.andExpect(
						MockMvcResultMatchers.jsonPath(jsonPath + ".tags[" + j + "]").value(message.getTags().get(j)));
			}
		} else {
			resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".tags").doesNotExist());
		}

		// Comparison between timestamps : now - dateCreation must be less than 10
		// seconds
		OffsetDateTime messageDateTime = null;
		if (index != null) {
			messageDateTime = ApiTestConstants.JSON_MAPPER
					.readValue(resultActions.andReturn().getResponse().getContentAsString(),
							new TypeReference<List<Message>>() {
							})
					.get(index).getDateCreation();
		} else {
			messageDateTime = ApiTestConstants.JSON_MAPPER
					.readValue(resultActions.andReturn().getResponse().getContentAsString(), Message.class)
					.getDateCreation();
		}
		Assertions.assertTrue(OffsetDateTime.now().toEpochSecond() - messageDateTime.toEpochSecond() < 10);
	}
}

package com.goubinthibault.microblog.tests.common;

/**
 * Common abstract tests class
 * 
 * @author Thibault GOUBIN
 */
public abstract class GenericTest {

	/**
	 * Tests engine and data initialization
	 * 
	 * @throws Exception an exception
	 */
	public abstract void init() throws Exception;

	/**
	 * Data initialization
	 * 
	 * @throws Exception an exception
	 */
	public void initData() throws Exception {
		// Nothing to do
	}
}

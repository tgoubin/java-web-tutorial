package com.goubinthibault.microblog.tests.common;

import java.time.OffsetDateTime;
import java.util.List;

import com.goubinthibault.microblog.api.dto.Message;
import com.goubinthibault.microblog.api.dto.ReactionMessage;
import com.goubinthibault.microblog.api.dto.Utilisateur;

/**
 * Factory for message objects
 * 
 * @author Thibault GOUBIN
 */
public class MessageFactory {

	/**
	 * Creates a Message instance
	 * 
	 * @param dateCreation the creation date
	 * @param id           the id
	 * @param reactions    the reactions
	 * @param tags         the tags
	 * @param texte        the texte
	 * @param utilisateur  the utilisateur
	 * @return the message
	 */
	public static Message createMessage(OffsetDateTime dateCreation, Integer id, List<ReactionMessage> reactions,
			List<String> tags, String texte, Utilisateur utilisateur) {
		Message message = new Message();
		message.setDateCreation(dateCreation);
		message.setId(id);
		message.setReactions(reactions);
		message.setTags(tags);
		message.setTexte(texte);
		message.setUtilisateur(utilisateur);
		return message;
	}
}

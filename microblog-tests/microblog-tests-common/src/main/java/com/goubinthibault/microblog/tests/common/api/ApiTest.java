package com.goubinthibault.microblog.tests.common.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.goubinthibault.microblog.api.MicroblogRestApplication;
import com.goubinthibault.microblog.tests.common.GenericTest;

/**
 * Common abstract API tests class
 * 
 * @author Thibault GOUBIN
 */
// SpringBoot test configuration
// Runs the SpringBoot application on a random port
// and autoconfigures MockMvc (API tests engine)
@SpringBootTest(classes = {
		MicroblogRestApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public abstract class ApiTest extends GenericTest {

	/**
	 * API tests engine
	 */
	@Autowired
	protected MockMvc mockMvc;

	/**
	 * Resource loader
	 */
	@Autowired
	protected ResourceLoader resourceLoader;

	/**
	 * JSON mapper and data initialization
	 * 
	 * @throws Exception an exception
	 */
	@Override
	@BeforeEach
	public void init() throws Exception {
		ApiTestUtils.initJSONMapper();
		initData();
	}
}

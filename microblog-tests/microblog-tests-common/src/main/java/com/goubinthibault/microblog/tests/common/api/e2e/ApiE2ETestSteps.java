package com.goubinthibault.microblog.tests.common.api.e2e;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.web.servlet.MockMvc;

import com.goubinthibault.microblog.tests.common.TestConstants;
import com.goubinthibault.microblog.tests.common.api.ApiTestUtils;

/**
 * Common abstract API E2E test steps class
 * 
 * @author Thibault GOUBIN
 */
public abstract class ApiE2ETestSteps {

	/**
	 * API tests engine
	 */
	@Autowired
	protected MockMvc mockMvc;

	/**
	 * Resource loader
	 */
	@Autowired
	protected ResourceLoader resourceLoader;

	/**
	 * The JDBC template
	 */
	@Autowired
	protected JdbcTemplate jdbcTemplate;

	/**
	 * JSON mapper and data initialization
	 * 
	 * @throws Exception an exception
	 */
	public void init() throws Exception {
		ApiTestUtils.initJSONMapper();

		ScriptUtils.executeSqlScript(jdbcTemplate.getDataSource().getConnection(),
				resourceLoader.getResource(TestConstants.RESET_SQL_DATA_FILE));
	}
}

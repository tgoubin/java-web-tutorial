package com.goubinthibault.microblog.tests.common.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.goubinthibault.microblog.api.MicroblogRestApplication;
import com.goubinthibault.microblog.tests.common.GenericTest;

/**
 * Common abstract repositories tests class
 * 
 * @author Thibault GOUBIN
 */
@SpringBootTest(classes = {
		MicroblogRestApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@AutoConfigureDataJpa
@AutoConfigureTestEntityManager
@ImportAutoConfiguration
public class RepositoryTest extends GenericTest {

	@Override
	@BeforeEach
	public void init() throws Exception {
		// Nothing to do
	}
}

package com.goubinthibault.microblog.tests.common.unit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.goubinthibault.microblog.tests.common.GenericTest;

/**
 * Common abstract unit tests class
 * 
 * @author Thibault GOUBIN
 */
@ExtendWith(MockitoExtension.class)
public abstract class UnitTest extends GenericTest {

	/**
	 * Mockito (mock engine) initialisation
	 * 
	 * @throws Exception an exception
	 */
	@Override
	@BeforeEach
	public void init() throws Exception {
		MockitoAnnotations.openMocks(this).close();
		initData();
	}
}

package com.goubinthibault.microblog.tests.common.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.goubinthibault.microblog.core.security.SecurityConstants;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

/**
 * API tests utilities
 * 
 * @author Thibault GOUBIN
 */
@SuppressWarnings("deprecation")
public class ApiTestUtils {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiTestUtils.class);

	/**
	 * JSON mapper initialization
	 */
	public static void initJSONMapper() {
		ApiTestConstants.JSON_MAPPER.registerModule(new JSR310Module());
		ApiTestConstants.JSON_MAPPER.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, false);
	}

	/**
	 * Execute a GET request
	 * 
	 * @param mockMvc the mock MVC
	 * @param path    the request path
	 * @param token   the token
	 * @return the result actions
	 * @throws Exception an Exception
	 */
	public static ResultActions get(MockMvc mockMvc, String path, String token) throws Exception {
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path)
				.contentType(MediaType.APPLICATION_JSON);

		if (token != null) {
			requestBuilder = requestBuilder.header(HttpHeaders.AUTHORIZATION, SecurityConstants.BEARER_PREFIX + token);
		}

		ResultActions resultActions = mockMvc.perform(requestBuilder);
		ApiTestUtils.logResponse(resultActions);
		return resultActions;
	}

	/**
	 * Execute a POST request
	 * 
	 * @param mockMvc     the mock MVC
	 * @param path        the request path
	 * @param jsonRequest the JSON request
	 * @return the result actions
	 * @throws Exception an Exception
	 */
	public static ResultActions post(MockMvc mockMvc, String path, String jsonRequest) throws Exception {
		ResultActions resultActions = mockMvc.perform(
				MockMvcRequestBuilders.post(path).contentType(MediaType.APPLICATION_JSON).content(jsonRequest));
		ApiTestUtils.logResponse(resultActions);
		return resultActions;
	}

	/**
	 * Execute a PUT request
	 * 
	 * @param mockMvc     the mock MVC
	 * @param path        the request path
	 * @param jsonRequest the JSON request
	 * @return the result actions
	 * @throws Exception an Exception
	 */
	public static ResultActions put(MockMvc mockMvc, String path, String jsonRequest) throws Exception {
		ResultActions resultActions = mockMvc
				.perform(MockMvcRequestBuilders.put(path).contentType(MediaType.APPLICATION_JSON).content(jsonRequest));
		ApiTestUtils.logResponse(resultActions);
		return resultActions;
	}

	/**
	 * Returns the HTTP request file content
	 * 
	 * @param resourceLoader the resource loader
	 * @param requestFile    the JSON request file
	 * @return the request file content
	 * @throws IOException an IOException
	 */
	public static String getRequestFileContent(ResourceLoader resourceLoader, String requestFile) throws IOException {
		Resource requestFileResource = resourceLoader
				.getResource(requestFile + ApiTestConstants.REQUEST_FILE_EXTENSION);
		return IOUtils.toString(requestFileResource.getInputStream(), StandardCharsets.UTF_8);
	}

	/**
	 * Returns a JSON value of an HTTP request result
	 * 
	 * @param resultActions the resource loader
	 * @param jsonKey       the JSON key
	 * @return the JSON value
	 * @throws UnsupportedEncodingException an UnsupportedEncodingException
	 * @throws ParseException               a ParseException
	 */
	public static String getRequestResultJSONValue(ResultActions resultActions, String jsonKey)
			throws UnsupportedEncodingException, ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(resultActions.andReturn().getResponse().getContentAsString());
		return json.get(jsonKey).toString();
	}

	/**
	 * Logs an HTTP response
	 * 
	 * @param resultActions the result actions
	 * @throws UnsupportedEncodingException an UnsupportedEncodingException
	 */
	public static void logResponse(ResultActions resultActions) throws UnsupportedEncodingException {
		LOGGER.info("HTTP response : {}", resultActions.andReturn().getResponse().getContentAsString());
	}

	/**
	 * Verification que Spring provoque bien une erreur 400 (BadRequest)
	 * 
	 * @param resultActions  l'objet ResultActions
	 * @param exceptionClass la classe d'exception
	 * @param errorMessage   la portion du message d'erreur attendu
	 */
	public static void verifySpringBadRequest(ResultActions resultActions, Class<? extends Exception> exceptionClass,
			String errorMessage) {
		Assertions.assertEquals(resultActions.andReturn().getResolvedException().getClass(), exceptionClass);
		Assertions.assertTrue(resultActions.andReturn().getResolvedException().getMessage().contains(errorMessage));
	}
}

package com.goubinthibault.microblog.tests.common;

import java.time.format.DateTimeFormatter;

/**
 * Tests constants
 * 
 * @author Thibault GOUBIN
 */
public class TestConstants {

	/**
	 * Date formatter
	 */
	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ISO_DATE;

	/**
	 * Date time formatter
	 */
	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_DATE_TIME;

	/**
	 * SQL file extension
	 */
	public static final String SQL_FILE_EXTENSION = ".sql";

	/**
	 * SQL data folder
	 */
	public static final String SQL_DATA_FOLDER = "file:src/test/resources/sql/";

	/**
	 * Reset SQL data file
	 */
	public static final String RESET_SQL_DATA_FILE = SQL_DATA_FOLDER + "reset_data" + SQL_FILE_EXTENSION;
}

package com.goubinthibault.microblog.tests.common;

import java.util.ArrayList;
import java.util.List;

import com.goubinthibault.microblog.api.dto.Utilisateur;
import com.goubinthibault.microblog.core.message.MessageEntity;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurEntity;

/**
 * Factory for utilisateur objects
 * 
 * @author Thibault GOUBIN
 */
public class UtilisateurFactory {

	/**
	 * Creates an Utilisateur instance
	 * 
	 * @param abonnements the abonnements
	 * @param abonnes     the abonnes
	 * @param email       the email
	 * @param id          the id
	 * @param login       the login
	 * @param nom         the nom
	 * @param prenom      the prenom
	 * @return the utilisateur
	 */
	public static Utilisateur createUtilisateur(List<Utilisateur> abonnements, List<Utilisateur> abonnes, String email,
			Integer id, String login, String nom, String prenom) {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setAbonnements(abonnements);
		utilisateur.setAbonnes(abonnes);
		utilisateur.setEmail(email);
		utilisateur.setId(id);
		utilisateur.setLogin(login);
		utilisateur.setNom(nom);
		utilisateur.setPrenom(prenom);
		return utilisateur;
	}

	/**
	 * Creates the anonymous utilisateur
	 * 
	 * @return the anonymous utilisateur
	 */
	public static Utilisateur createUtilisateurAnonymous() {
		Integer idUtilisateur = 1;
		String login = "anonymous";
		String nom = "TEST";
		String prenom = "Test";
		String email = "test@outlook.com";
		return UtilisateurFactory.createUtilisateur(new ArrayList<>(), new ArrayList<>(), email, idUtilisateur, login,
				nom, prenom);
	}

	/**
	 * Creates an Utilisateur entity instance
	 * 
	 * @param abonnements the abonnements
	 * @param abonnes     the abonnes
	 * @param email       the email
	 * @param id          the id
	 * @param login       the login
	 * @param message     the messages
	 * @param motDePasse  the mot de passe
	 * @param nom         the nom
	 * @param prenom      the prenom
	 * @return the utilisateur entity
	 */
	public static UtilisateurEntity createUtilisateurEntity(List<UtilisateurEntity> abonnements,
			List<UtilisateurEntity> abonnes, String email, Integer id, String login, List<MessageEntity> messages,
			String motDePasse, String nom, String prenom) {
		UtilisateurEntity utilisateur = new UtilisateurEntity();
		utilisateur.setAbonnements(abonnements);
		utilisateur.setAbonnes(abonnes);
		utilisateur.setEmail(email);
		utilisateur.setId(id);
		utilisateur.setLogin(login);
		utilisateur.setMessages(messages);
		utilisateur.setMotDePasse(motDePasse);
		utilisateur.setNom(nom);
		utilisateur.setPrenom(prenom);
		return utilisateur;
	}
}

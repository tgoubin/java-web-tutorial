package com.goubinthibault.microblog.tests.common.api;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * API tests constants
 * 
 * @author Thibault GOUBIN
 */
public class ApiTestConstants {

	/**
	 * HTTP requests folder
	 */
	public static final String REQUESTS_FOLDER = "file:src/test/resources/requests";

	/**
	 * HTTP requests extension
	 */
	public static final String REQUEST_FILE_EXTENSION = ".json";

	/**
	 * JSON mapper
	 */
	public static final ObjectMapper JSON_MAPPER = new ObjectMapper();

	/**
	 * Marker for JSON templates
	 */
	public static final String TEMPLATE_MARKER = "%";
}

package com.goubinthibault.microblog.tests.common;

import com.goubinthibault.microblog.api.dto.ReactionMessage;
import com.goubinthibault.microblog.api.dto.ReactionMessage.ReactionEnum;
import com.goubinthibault.microblog.api.dto.Utilisateur;

/**
 * Factory for reactionMessage objects
 * 
 * @author Thibault GOUBIN
 */
public class ReactionMessageFactory {

	/**
	 * Creates a ReactionMessage instance
	 * 
	 * @param reaction    the reaction
	 * @param utilisateur the utilisateur
	 * @return the reactionMessage
	 */
	public static ReactionMessage createReactionMessage(ReactionEnum reaction, Utilisateur utilisateur) {
		ReactionMessage reactionMessage = new ReactionMessage();
		reactionMessage.setReaction(reaction);
		reactionMessage.setUtilisateur(utilisateur);
		return reactionMessage;
	}
}

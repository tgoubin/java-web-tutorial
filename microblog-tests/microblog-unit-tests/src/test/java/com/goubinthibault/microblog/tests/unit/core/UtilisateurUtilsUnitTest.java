package com.goubinthibault.microblog.tests.unit.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.goubinthibault.microblog.core.utilisateur.UtilisateurUtils;
import com.goubinthibault.microblog.tests.common.unit.UnitTest;

/**
 * Unit tests for 'UtilisateurUtils'
 * 
 * @author Thibault GOUBIN
 */
public class UtilisateurUtilsUnitTest extends UnitTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilisateurUtilsUnitTest.class);

	/**
	 * Unit test for 'UtilisateurUtils::encodeMotDePasse()' - OK
	 */
	@Test
	public void encodeMotDePasse_OK() {
		LOGGER.info("Unit test for 'UtilisateurUtils::encodeMotDePasse()' - OK");

		String expectedMotDePasse = "QUZFOTYxQjVGRjA1REExQTMzNUFCQzgwQzhDMDBCNjY=";

		Assertions.assertEquals(expectedMotDePasse, UtilisateurUtils.encodeMotDePasse("motDePasseTest"));
	}
}

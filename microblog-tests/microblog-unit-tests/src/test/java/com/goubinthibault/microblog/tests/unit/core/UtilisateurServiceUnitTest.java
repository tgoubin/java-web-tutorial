package com.goubinthibault.microblog.tests.unit.core;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

import com.goubinthibault.microblog.core.exception.ForbiddenException;
import com.goubinthibault.microblog.core.exception.NotFoundException;
import com.goubinthibault.microblog.core.security.MicroblogAuthenticationToken;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurEntity;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurRepository;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurService;
import com.goubinthibault.microblog.tests.common.UtilisateurFactory;
import com.goubinthibault.microblog.tests.common.unit.UnitTest;

/**
 * Unit tests for 'UtilisateurService'
 * 
 * @author Thibault GOUBIN
 */
public class UtilisateurServiceUnitTest extends UnitTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilisateurServiceUnitTest.class);

	/**
	 * Token service to test
	 */
	@InjectMocks
	private UtilisateurService utilisateurService;

	/**
	 * Repository mocked for "utilisateur" entity
	 */
	@Mock
	private UtilisateurRepository utilisateurRepository;

	/**
	 * Unit test for 'UtilisateurService::getById()' - OK
	 */
	@Test
	public void getById_OK() {
		LOGGER.info("Unit test for 'UtilisateurService::getById()' - OK");

		Integer id = 1;
		String login = "test";
		UtilisateurEntity utilisateur = UtilisateurFactory.createUtilisateurEntity(new ArrayList<>(), new ArrayList<>(),
				"test@gmail.com", id, login, new ArrayList<>(), "", "TEST", "Test");

		// 'UtilisateurRepository::findById(...)' simulation
		Mockito.when(utilisateurRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(utilisateur));

		// Authentication saving in the security context
		SecurityContextHolder.getContext().setAuthentication(new MicroblogAuthenticationToken(login));

		// Method to test ('UtilisateurService::getById()') call
		UtilisateurEntity utilisateurReturned = utilisateurService.getById(id);

		// utilisateurReturned content checks
		Assertions.assertEquals(utilisateur.getAbonnements(), utilisateurReturned.getAbonnements());
		Assertions.assertEquals(utilisateur.getAbonnes(), utilisateurReturned.getAbonnes());
		Assertions.assertEquals(utilisateur.getEmail(), utilisateurReturned.getEmail());
		Assertions.assertEquals(utilisateur.getLogin(), utilisateurReturned.getLogin());
		Assertions.assertEquals(utilisateur.getMessages(), utilisateurReturned.getMessages());
		Assertions.assertEquals(utilisateur.getMotDePasse(), utilisateurReturned.getMotDePasse());
		Assertions.assertEquals(utilisateur.getNom(), utilisateurReturned.getNom());
		Assertions.assertEquals(utilisateur.getPrenom(), utilisateurReturned.getPrenom());

		// Checks that 'UtilisateurRepository::findById(...)' is called 1 time
		Mockito.verify(utilisateurRepository, Mockito.times(1)).findById(ArgumentMatchers.any());
	}

	/**
	 * Unit test for 'UtilisateurService::getById()' - NotFoundException
	 */
	@Test
	public void getById_KO_NotFoundException() {
		LOGGER.info("Unit test for 'UtilisateurService::getById()' - NotFoundException");

		Integer id = 1;

		// 'UtilisateurRepository::findById(...)' simulation
		Mockito.when(utilisateurRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.ofNullable(null));

		// Checks that 'UtilisateurService::getById(...)' throws a NotFoundException
		Assertions.assertThrows(NotFoundException.class, () -> {
			utilisateurService.getById(id);
		}, "Utilisateur with id '" + id + "' does not exist");

		// Checks that 'UtilisateurRepository::findById(...)' is called 1 time
		Mockito.verify(utilisateurRepository, Mockito.times(1)).findById(ArgumentMatchers.any());
	}

	/**
	 * Unit test for 'UtilisateurService::getById()' - ForbiddenException
	 */
	@Test
	public void getById_KO_ForbiddenException() {
		LOGGER.info("Unit test for 'UtilisateurService::getById()' - ForbiddenException");

		Integer id = 1;
		String login = "test";
		String login2 = "test2";
		UtilisateurEntity utilisateur = UtilisateurFactory.createUtilisateurEntity(new ArrayList<>(), new ArrayList<>(),
				"test@gmail.com", id, login, new ArrayList<>(), "", "TEST", "Test");

		// 'UtilisateurRepository::findById(...)' simulation
		Mockito.when(utilisateurRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(utilisateur));

		// Authentication saving (with other login) in the security context
		SecurityContextHolder.getContext().setAuthentication(new MicroblogAuthenticationToken(login2));

		// Checks that 'UtilisateurService::getById(...)' throws a ForbiddenException
		Assertions.assertThrows(ForbiddenException.class, () -> {
			utilisateurService.getById(id);
		}, "Utilisateur with login '" + login2 + "' cannot access Utilisateur with id '" + id.toString() + "'");

		// Checks that 'UtilisateurRepository::findById(...)' is called 1 time
		Mockito.verify(utilisateurRepository, Mockito.times(1)).findById(ArgumentMatchers.any());
	}
}

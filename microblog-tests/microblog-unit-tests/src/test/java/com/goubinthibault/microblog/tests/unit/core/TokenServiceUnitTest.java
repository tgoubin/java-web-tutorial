package com.goubinthibault.microblog.tests.unit.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import com.goubinthibault.microblog.core.security.TokenService;
import com.goubinthibault.microblog.tests.common.unit.UnitTest;

/**
 * Unit tests for 'TokenService'
 * 
 * @author Thibault GOUBIN
 */
public class TokenServiceUnitTest extends UnitTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenServiceUnitTest.class);

	private static final String SECRET = "secret";
	private static final String MICROBLOG_SECRET = "microblogsecret";

	/**
	 * Token service to test
	 */
	@InjectMocks
	private TokenService tokenService;

	/**
	 * Unit test for 'TokenService::generateToken()' and
	 * 'TokenService::validateToken()' - OK
	 */
	@Test
	public void generateToken_validateToken_OK() {
		LOGGER.info("Unit test for 'TokenService::generateToken()', 'TokenService::validateToken()' - OK");

		String login = "test";

		ReflectionTestUtils.setField(tokenService, SECRET, MICROBLOG_SECRET);
		String token = tokenService.generateToken(login);

		Assertions.assertNotNull(token);
		Assertions.assertEquals(tokenService.validateToken(token), login);
	}

	/**
	 * Unit test for 'TokenService::generateToken()' and
	 * 'TokenService::validateToken()' - KO - Expired
	 */
	@Test
	public void generateToken_validateToken_KO_Expired() {
		LOGGER.info("Unit test for 'TokenService::generateToken()' and 'TokenService::validateToken()' - KO - Expired");

		ReflectionTestUtils.setField(tokenService, SECRET, MICROBLOG_SECRET);
		String login = "test";

		// Token generation with a very short expiration time
		String token = tokenService.generateToken(login, System.currentTimeMillis() - 10,
				System.currentTimeMillis() + 1);

		Assertions.assertNotNull(token);
		Assertions.assertNull(tokenService.validateToken(token));
	}

	/**
	 * 'TokenService::generateToken()' - KO - Invalid
	 */
	@Test
	public void validateToken_KO_Invalid() {
		LOGGER.info("Unit test for 'TokenService::validateToken()' - KO - Invalid");

		ReflectionTestUtils.setField(tokenService, SECRET, MICROBLOG_SECRET);
		String token = "invalidtoken";

		Assertions.assertNull(tokenService.validateToken(token));
	}
}

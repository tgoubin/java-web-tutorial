package com.goubinthibault.microblog.tests.api.e2e.steps;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;

import com.goubinthibault.microblog.api.login.LoginController;
import com.goubinthibault.microblog.core.security.TokenService;
import com.goubinthibault.microblog.tests.api.e2e.common.CommonApiE2ETestConstants;
import com.goubinthibault.microblog.tests.api.e2e.common.DescriptionLogin;
import com.goubinthibault.microblog.tests.api.e2e.common.DescriptionUtilisateur;
import com.goubinthibault.microblog.tests.common.TestConstants;
import com.goubinthibault.microblog.tests.common.api.ApiTestConstants;
import com.goubinthibault.microblog.tests.common.api.ApiTestUtils;
import com.goubinthibault.microblog.tests.common.api.e2e.ApiE2ETestSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * Common steps for API E2E tests
 * 
 * @author Thibault GOUBIN
 */
public class CommonTestSteps extends ApiE2ETestSteps {

	/**
	 * Service for "token"
	 */
	@Autowired
	private TokenService tokenService;

	private static final String COMMON_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER + "/common";
	private static final String POST_COMMON_REQUESTS_FOLDER = COMMON_REQUESTS_FOLDER + "/post/";

	/**
	 * "I initialize some utilisateurs with the following data :" step associated
	 * method
	 * 
	 * @param descriptionsUtilisateursDataTable utilisateurs descriptions data table
	 * @throws Exception an Exception
	 */
	@Given("I initialize some utilisateurs with the following data :")
	public void initUtilisateurs(DataTable descriptionsUtilisateursDataTable) throws Exception {
		init();

		List<DescriptionUtilisateur> descriptionsUtilisateurs = DescriptionUtilisateur
				.buildDescriptionsUtilisateurs(descriptionsUtilisateursDataTable);

		for (DescriptionUtilisateur descriptionUtilisateur : descriptionsUtilisateurs) {
			// Insertion of utilisateur by executing a SQL script, written from a template
			jdbcTemplate
					.execute(
							DescriptionUtilisateur.buildUtilisateurRequestFromDescriptionAndTemplate(IOUtils.toString(
									resourceLoader.getResource(TestConstants.SQL_DATA_FOLDER + "utilisateur_template"
											+ TestConstants.SQL_FILE_EXTENSION).getInputStream(),
									StandardCharsets.UTF_8), descriptionUtilisateur));
		}
	}

	/**
	 * "I try to log in with the following data :" step associated method
	 * 
	 * @param descriptionLogInDataTable log in description data table
	 * @throws Exception an Exception
	 */
	@When("I try to log in with the following data :")
	public void logIn(DataTable descriptionLogInDataTable) throws Exception {
		// POST /login execution, with a JSON written from a template
		MockHttpServletResponse response = ApiTestUtils
				.post(mockMvc, LoginController.API_PATH,
						DescriptionLogin.buildLogInJsonRequestFromDescriptionAndTemplate(
								ApiTestUtils.getRequestFileContent(resourceLoader,
										POST_COMMON_REQUESTS_FOLDER + "login_template"),
								DescriptionLogin.buildDescriptionLogin(descriptionLogInDataTable)))
				.andReturn().getResponse();

		CommonApiE2ETestConstants.HTTP_RESPONSE_CODE = response.getStatus();
		if (CommonApiE2ETestConstants.HTTP_RESPONSE_CODE == HttpStatus.OK.value()) {
			// If authentication OK, we store the token
			CommonApiE2ETestConstants.TOKEN = response.getContentAsString();
		} else {
			CommonApiE2ETestConstants.TOKEN = null;
		}
	}

	/**
	 * "I check that the HTTP response code is {int}" step associated method
	 * 
	 * @param httpResponseCode the HTTP response code
	 */
	@Then("I check that the HTTP response code is {int}")
	public void checkHTTPResponseCode(int httpResponseCode) {
		Assertions.assertTrue(httpResponseCode == CommonApiE2ETestConstants.HTTP_RESPONSE_CODE);
	}

	/**
	 * "I check that the token returned corresponds to utilisateur {string}" step
	 * associated method
	 * 
	 * @param login the login
	 */
	@Then("I check that the token returned corresponds to utilisateur {string}")
	public void checkLoginInToken(String login) {
		Assertions.assertEquals(login, tokenService.validateToken(CommonApiE2ETestConstants.TOKEN));
	}

	/**
	 * "I check that the HTTP response contains {string}" step associated method
	 * 
	 * @param stringToCheck the string to check
	 */
	@Then("I check that the HTTP response contains {string}")
	public void checkHTTPResponseContains(String stringToCheck) {
		Assertions.assertTrue(CommonApiE2ETestConstants.HTTP_RESPONSE.contains(stringToCheck));
	}
}

package com.goubinthibault.microblog.tests.api.e2e;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.goubinthibault.microblog.api.MicroblogRestApplication;

import io.cucumber.spring.CucumberContextConfiguration;

/**
 * Cucumber configuration class
 * 
 * @author Thibault GOUBIN
 */
@SpringBootTest(classes = {
		MicroblogRestApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@CucumberContextConfiguration
@AutoConfigureMockMvc
public class CucumberConfiguration {
}

package com.goubinthibault.microblog.tests.api.e2e.steps;

import org.springframework.mock.web.MockHttpServletResponse;

import com.goubinthibault.microblog.api.utilisateur.UtilisateurController;
import com.goubinthibault.microblog.tests.api.e2e.common.CommonApiE2ETestConstants;
import com.goubinthibault.microblog.tests.common.api.ApiTestUtils;
import com.goubinthibault.microblog.tests.common.api.e2e.ApiE2ETestSteps;

import io.cucumber.java.en.When;

/**
 * Utilisateur steps for API E2E tests
 * 
 * @author Thibault GOUBIN
 */
public class UtilisateurTestSteps extends ApiE2ETestSteps {

	/**
	 * "I try to access the utilisateur with id {int}" step associated method
	 * 
	 * @param id the utilisateur id
	 * @throws Exception an Exception
	 */
	@When("I try to access the utilisateur with id {int}")
	public void getUtilisateurById(int id) throws Exception {
		MockHttpServletResponse response = ApiTestUtils
				.get(mockMvc, UtilisateurController.API_PATH + "/" + id, CommonApiE2ETestConstants.TOKEN).andReturn()
				.getResponse();

		CommonApiE2ETestConstants.HTTP_RESPONSE_CODE = response.getStatus();
		CommonApiE2ETestConstants.HTTP_RESPONSE = response.getContentAsString();
	}
}

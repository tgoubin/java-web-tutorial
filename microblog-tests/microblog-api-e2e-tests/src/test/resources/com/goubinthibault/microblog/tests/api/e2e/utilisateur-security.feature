Feature: UtilisateurSecurity

  Scenario: Check that an "utilisateur" correctly authenticated can do only its authorized actions
    Given I initialize some utilisateurs with the following data :
      |id|login|motDePasse|nom   |prenom|email            |
      |  |test |test      |TEST  |Test  |test@gmail.com   |
      |  |test2|test2     |TEST 2|Test 2|test2@outlook.com|
      |  |test3|test3     |TEST 3|Test 3|test3@yahoo.fr   |
    When I try to log in with the following data :
      |login|motDePasse|
      |test2|test2     |
    Then I check that the HTTP response code is 200
    And I check that the token returned corresponds to utilisateur "test2"
    When I try to access the utilisateur with id 2
    Then I check that the HTTP response code is 200 
    And I check that the HTTP response contains "\"nom\":\"TEST 2\""
    When I try to access the utilisateur with id 3
    Then I check that the HTTP response code is 403
    And I check that the HTTP response contains "cannot access" 
  
  Scenario: Check that an "utilisateur" not correctly authenticated cannot actions
    Given I initialize some utilisateurs with the following data :
      |id|login|motDePasse|nom   |prenom|email            |
      |  |test |test      |TEST  |Test  |test@gmail.com   |
      |  |test2|test2     |TEST 2|Test 2|test2@outlook.com|
      |  |test3|test3     |TEST 3|Test 3|test3@yahoo.fr   |
    When I try to log in with the following data :
      |login|motDePasse|
      |test3|test2     |
    Then I check that the HTTP response code is 401
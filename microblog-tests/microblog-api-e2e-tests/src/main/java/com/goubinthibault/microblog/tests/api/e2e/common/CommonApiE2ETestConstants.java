package com.goubinthibault.microblog.tests.api.e2e.common;

/**
 * Constants for API E2E tests
 * 
 * @author Thibault GOUBIN
 */
public class CommonApiE2ETestConstants {

	public static Integer HTTP_RESPONSE_CODE = null;
	public static String HTTP_RESPONSE = null;
	public static String TOKEN = null;
}

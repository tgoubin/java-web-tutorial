package com.goubinthibault.microblog.tests.api.e2e.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.goubinthibault.microblog.core.utilisateur.UtilisateurUtils;
import com.goubinthibault.microblog.tests.common.api.ApiTestConstants;

import io.cucumber.datatable.DataTable;

/**
 * Utilisateur description class
 * 
 * @author Thibault GOUBIN
 */
public class DescriptionUtilisateur {

	private final Integer id;
	private final String login;
	private final String motDePasse;
	private final String nom;
	private final String prenom;
	private final String email;

	private static final String ID = "id";
	private static final String LOGIN = "login";
	private static final String MOT_DE_PASSE = "motDePasse";
	private static final String NOM = "nom";
	private static final String PRENOM = "prenom";
	private static final String EMAIL = "email";

	/**
	 * Constructor
	 * 
	 * @param id     the id
	 * @param login  the login
	 * @param nom    the nom
	 * @param prenom the prenom
	 * @param email  the email
	 */
	public DescriptionUtilisateur(Integer id, String login, String motDePasse, String nom, String prenom,
			String email) {
		this.id = id;
		this.login = login;
		this.motDePasse = motDePasse;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
	}

	/**
	 * Build utilisateurs descriptions list from utilisateurs descriptions data
	 * table
	 * 
	 * @param descriptionsUtilisateursDataTable utilisateurs descriptions data table
	 * @return the utilisateurs descriptions list
	 */
	public static List<DescriptionUtilisateur> buildDescriptionsUtilisateurs(
			DataTable descriptionsUtilisateursDataTable) {
		List<Map<String, String>> descriptionsUtilisateursList = descriptionsUtilisateursDataTable.asMaps();

		List<DescriptionUtilisateur> descriptionsUtilisateurs = new ArrayList<>();
		for (Map<String, String> descriptionUtilisateur : descriptionsUtilisateursList) {
			Integer id = (descriptionUtilisateur.containsKey(ID) && descriptionUtilisateur.get(ID) != null
					&& StringUtils.isNotBlank(descriptionUtilisateur.get(ID).trim()))
							? Integer.parseInt(descriptionUtilisateur.get(ID).trim())
							: null;

			descriptionsUtilisateurs.add(new DescriptionUtilisateur(id, descriptionUtilisateur.get(LOGIN).trim(),
					descriptionUtilisateur.get(MOT_DE_PASSE).trim(), descriptionUtilisateur.get(NOM).trim(),
					descriptionUtilisateur.get(PRENOM).trim(), descriptionUtilisateur.get(EMAIL).trim()));
		}

		return descriptionsUtilisateurs;
	}

	/**
	 * Build utilisateur JSON/SQL request from utilisateur description and a
	 * template
	 * 
	 * @param utilisateurTemplate    the utilisateur JSON request template
	 * @param descriptionUtilisateur the utilisateur description
	 * @return the utilisateur JSON request
	 */
	public static String buildUtilisateurRequestFromDescriptionAndTemplate(String utilisateurTemplate,
			DescriptionUtilisateur descriptionUtilisateur) {
		String utilisateurRequest = utilisateurTemplate.replaceAll(
				ApiTestConstants.TEMPLATE_MARKER + ID + ApiTestConstants.TEMPLATE_MARKER,
				(descriptionUtilisateur.getId() != null) ? String.valueOf(descriptionUtilisateur.getId()) : "null");
		utilisateurRequest = utilisateurRequest.replaceAll(
				ApiTestConstants.TEMPLATE_MARKER + LOGIN + ApiTestConstants.TEMPLATE_MARKER,
				descriptionUtilisateur.getLogin());

		if (descriptionUtilisateur.getMotDePasse() != null) {
			utilisateurRequest = utilisateurRequest.replaceAll(
					ApiTestConstants.TEMPLATE_MARKER + MOT_DE_PASSE + ApiTestConstants.TEMPLATE_MARKER,
					UtilisateurUtils.encodeMotDePasse(descriptionUtilisateur.getMotDePasse()));
		}

		utilisateurRequest = utilisateurRequest.replaceAll(
				ApiTestConstants.TEMPLATE_MARKER + NOM + ApiTestConstants.TEMPLATE_MARKER,
				descriptionUtilisateur.getNom());
		utilisateurRequest = utilisateurRequest.replaceAll(
				ApiTestConstants.TEMPLATE_MARKER + PRENOM + ApiTestConstants.TEMPLATE_MARKER,
				descriptionUtilisateur.getPrenom());
		utilisateurRequest = utilisateurRequest.replaceAll(
				ApiTestConstants.TEMPLATE_MARKER + EMAIL + ApiTestConstants.TEMPLATE_MARKER,
				descriptionUtilisateur.getEmail());

		return utilisateurRequest;
	}

	public Integer getId() {
		return id;
	}

	public String getLogin() {
		return login;
	}

	public String getNom() {
		return nom;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getEmail() {
		return email;
	}
}

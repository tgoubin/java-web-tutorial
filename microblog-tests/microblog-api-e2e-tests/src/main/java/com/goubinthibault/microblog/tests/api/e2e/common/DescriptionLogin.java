package com.goubinthibault.microblog.tests.api.e2e.common;

import java.util.Map;

import com.goubinthibault.microblog.tests.common.api.ApiTestConstants;

import io.cucumber.datatable.DataTable;

/**
 * Login description class
 * 
 * @author Thibault GOUBIN
 */
public class DescriptionLogin {

	private final String login;
	private final String motDePasse;

	private static final String LOGIN = "login";
	private static final String MOT_DE_PASSE = "motDePasse";

	/**
	 * Constructor
	 * 
	 * @param login      the login
	 * @param motDePasse the mot de passe
	 */
	public DescriptionLogin(String login, String motDePasse) {
		this.login = login;
		this.motDePasse = motDePasse;
	}

	/**
	 * Build logIn description from logIn description data table
	 * 
	 * @param descriptionLogInDataTable logIn description data table
	 * @return the logIn description
	 */
	public static DescriptionLogin buildDescriptionLogin(DataTable descriptionLogInDataTable) {
		Map<String, String> logInDataMap = descriptionLogInDataTable.asMaps().get(0);
		return new DescriptionLogin(logInDataMap.get(LOGIN), logInDataMap.get(MOT_DE_PASSE));
	}

	/**
	 * Build logIn JSON request from logIn description and a template
	 * 
	 * @param logInTemplate    the logIn template
	 * @param descriptionLogin the logIn description
	 * @return the logIn JSON request
	 */
	public static String buildLogInJsonRequestFromDescriptionAndTemplate(String logInTemplate,
			DescriptionLogin descriptionLogin) {
		String logInRequest = logInTemplate.replaceAll(
				ApiTestConstants.TEMPLATE_MARKER + LOGIN + ApiTestConstants.TEMPLATE_MARKER,
				descriptionLogin.getLogin());
		logInRequest = logInRequest.replaceAll(
				ApiTestConstants.TEMPLATE_MARKER + MOT_DE_PASSE + ApiTestConstants.TEMPLATE_MARKER,
				descriptionLogin.getMotDePasse());

		return logInRequest;
	}

	public String getLogin() {
		return login;
	}

	public String getMotDePasse() {
		return motDePasse;
	}
}

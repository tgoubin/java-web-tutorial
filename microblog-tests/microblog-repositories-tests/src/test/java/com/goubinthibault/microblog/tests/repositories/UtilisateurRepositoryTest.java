package com.goubinthibault.microblog.tests.repositories;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import com.goubinthibault.microblog.core.utilisateur.UtilisateurEntity;
import com.goubinthibault.microblog.core.utilisateur.UtilisateurRepository;
import com.goubinthibault.microblog.tests.common.TestConstants;
import com.goubinthibault.microblog.tests.common.repositories.RepositoryTest;

/**
 * Tests for 'UtilisateurRepository'
 * 
 * @author Thibault GOUBIN
 */
public class UtilisateurRepositoryTest extends RepositoryTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilisateurRepositoryTest.class);

	/**
	 * Repository to test
	 */
	@Autowired
	private UtilisateurRepository utilisateurRepository;

	/**
	 * Test for 'UtilisateurRepositoryTest::findByLogin()'
	 */
	@Test
	@Sql(scripts = { TestConstants.RESET_SQL_DATA_FILE,
			UtilisateurRepositoryTestConstants.UTILISATEUR_REPOSITORY_SQL_DATA_FOLDER + "findByLogin"
					+ TestConstants.SQL_FILE_EXTENSION })
	public void tests_findByLogin() {
		LOGGER.info("Test for 'UtilisateurRepositoryTest::findByLogin()'");

		test_findByLogin("test", true);
		test_findByLogin("test2", true);
		test_findByLogin("test3", false);
	}

	/**
	 * Test for 'UtilisateurRepositoryTest::findByLogin()'
	 * 
	 * @param login            the login
	 * @param isResultExpected if a result is expected or not
	 */
	private void test_findByLogin(String login, boolean isResultExpected) {
		Optional<UtilisateurEntity> utilisateur = utilisateurRepository.findByLogin(login);

		Assertions.assertTrue(isResultExpected == utilisateur.isPresent());

		if (isResultExpected) {
			Assertions.assertEquals(login, utilisateur.get().getLogin());
		}
	}
}

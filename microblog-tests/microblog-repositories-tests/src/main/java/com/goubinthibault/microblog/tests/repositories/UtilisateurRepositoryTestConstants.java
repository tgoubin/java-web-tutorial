package com.goubinthibault.microblog.tests.repositories;

import com.goubinthibault.microblog.tests.common.TestConstants;

/**
 * Utilisateur repository tests constants
 * 
 * @author Thibault GOUBIN
 */
public class UtilisateurRepositoryTestConstants {

	/**
	 * UtilisateurRepositoryTest SQL data folder:
	 * "src/test/resources/sql/utilisateurRepository"
	 */
	public final static String UTILISATEUR_REPOSITORY_SQL_DATA_FOLDER = TestConstants.SQL_DATA_FOLDER
			+ "utilisateurRepository/";
}
